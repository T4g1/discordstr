import asyncio
import discord
import random

from discord.utils import find

from bot import bot
from classes.exception import *
from units import heros, madsoldier, minion, soldier
from buildings import barracks, farm, granary, mine, quarry, sawmill, treasury, warehouse


l_buildings = [
    barracks.Barracks,
    farm.Farm,
    granary.Granary,
    mine.Mine,
    quarry.Quarry,
    sawmill.Sawmill,
    treasury.Treasury,
    warehouse.Warehouse,
]


l_units = [
    minion.Minion,
    soldier.Soldier,
    madsoldier.MadSoldier,
    heros.Heros,
]


def get_building_class(name):
    """Gets a building class after its name"""
    for building_class in l_buildings:
        if building_class.name.lower() == name.lower():
            return building_class
    
    raise UnknwonBuilding(name)


def get_unit_class(name):
    """Gets an unit class after its name"""
    for unit_class in l_units:
        if unit_class.name.lower() == name.lower():
            return unit_class
    
    raise UnknwonUnit(name)


def kingdom_required(author, users):
    """Check if the user posses a kingdom or not"""
    if not author.id in users or users[author.id].kingdom is None:
        raise UserHasNoKingdom(author)
    
    return (users[author.id], users[author.id].kingdom)


def workforce_required(author, users, workforce):
    """Check if the user posses the required workforce or not"""
    user, kingdom = kingdom_required(author, users)
    
    if workforce <= 0:
        raise NotPositiveInteger(workforce)
    
    # Not enough workers
    if kingdom.workforce < workforce:
        raise NotEnoughWorkforce(has=kingdom.workforce, needed=workforce)
    
    return (user, kingdom)


def check_buildings_requierements(kingdom, unit):
    """Raise a BuildingsRequieremntsNotMet exception if the
    user don't have required buildings to build the unit
    """
    missing_buildings = []
    
    for required_building in unit.required_building:
        if not kingdom.own_building(required_building):
            missing_buildings.append(required_building)

    if missing_buildings:
        raise BuildingsRequieremntsNotMet(missing_buildings)


async def whisper(user, message):
    """Send a whisper to that user"""
    if user is not None:
        user_test = discord.user.User(username=user.name, mention=user.mention, id=user.id)
        await bot.send_message(user_test, message)


def get_user(name, members):
    member = find(lambda m: m.name == name, members)
    if member is None:
        raise Exception("No member with this name found.")
    return member


def get_lack_str(lack=None):
    if lack is not None:
        line = "You lack of: {gold} Gold, {food} Food, {wood} Wood, {stone} Stone".format(gold=str(lack[0]), food = str(lack[1]), wood = str(lack[2]), stone = str(lack[3]))
        return line


def add_new_random_map_position(worldmap, owner=""):
    """ Return a new free random position in the map """
    x, y = random.randrange(0, worldmap.width), random.randrange(0, worldmap.height)
    while 1:
        try:
            x, y = random.randrange(0, worldmap.width), random.randrange(0, worldmap.height)
            worldmap.add_position(owner, x, y)
        # Break the infinite while and raise the exception a level higher
        except WorldMapIsFull:
            raise WorldMapIsFull()
        except (WorldMapPositionAlreadyOwned, Exception):
            pass
        else:
            return x, y
