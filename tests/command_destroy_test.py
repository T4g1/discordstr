from test import DiscordSTRTestCase
from commands.destroy import destroy as _destroy
from common import *


class CommandDestroyTestCase(DiscordSTRTestCase):
    def test_destroy(self):
        """Destroying our kingdom"""
        self.generate_kingdom()
        try:
            self.get_kingdom()
        except UserHasNoKingdom:
            self.fail("UserHasNoKingdom was raised")
        
        with self.assertRaises(UserHasNoKingdom):
            self.loop.run_until_complete(_destroy.callback(self.context, "greenlamp_sucks"))
            
            self.get_kingdom()
    
    def test_destroy_without_kingdom(self):
        """Destroying a kingdom while we have none"""
        with self.assertRaises(UserHasNoKingdom):
            self.loop.run_until_complete(_destroy.callback(self.context, "greenlamp_sucks"))

            self.get_kingdom()
    
    def test_destroy_without_confirmation(self):
        """Destroying a kingdom while we have none"""
        self.generate_kingdom()

        try:
            self.loop.run_until_complete(_destroy.callback(self.context))

            self.get_kingdom()
        except UserHasNoKingdom:
            self.fail("UserHasNoKingdom was raised")
