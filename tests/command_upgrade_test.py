from test import DiscordSTRTestCase
from commands.upgrade import upgrade as _upgrade
from classes.ressources import Ressources
from common import *


class CommandUpgradeTestCase(DiscordSTRTestCase):
    def setUp(self):
        """Initialise a basic Kingdom"""
        super(CommandUpgradeTestCase, self).setUp()
        
        self.generate_kingdom()
    
    def test_upgrade(self):
        """Try to upgrade every buildings with requirements met"""
        self.build_all_buildings()
        
        kingdom = self.get_kingdom()
        
        for building_class in l_buildings:
            building = kingdom.get_building(building_class)
            kingdom._ressources = building.get_upgrade_cost()
            self.large_workforce()
            
            target_level = building.level + 1
            
            self.loop.run_until_complete(_upgrade.callback(self.context, building_class.name))
            
            self.assertEqual(kingdom.get_building(building_class).level, target_level - 1)
            
            self.end_tasks()

            self.assertEqual(kingdom.get_building(building_class).level, target_level)
    
    def test_upgrade_without_ressources(self):
        """Try to upgrade  without ressources"""
        self.build_all_buildings()
        
        kingdom = self.get_kingdom()
        
        for building_class in l_buildings:
            # Don't do that test for buildings that upgrade themself without workforce (wtf ?)
            if building_class.builders_needed <= 0:
                pass

            building = kingdom.get_building(building_class)
            kingdom._ressources = Ressources()
            self.large_workforce()
            
            target_level = building.level + 1
            
            self.loop.run_until_complete(_upgrade.callback(self.context, building_class.name))

            self.assertEqual(kingdom.get_building(building_class).level, target_level - 1)
    
    def test_upgrade_without_workforce(self):
        """Try to upgrade without minions to build it"""
        self.build_all_buildings()
        
        kingdom = self.get_kingdom()
        
        for building_class in l_buildings:
            building = kingdom.get_building(building_class)
            kingdom._ressources = building.get_upgrade_cost()
            
            target_level = building.level + 1
            
            self.loop.run_until_complete(_upgrade.callback(self.context, building_class.name))

            self.assertEqual(kingdom.get_building(building_class).level, target_level - 1)
    
    def test_upgrade_without_building(self):
        """Try to upgrade a building that is not built yet"""
        kingdom = self.get_kingdom()
        
        for building_class in l_buildings:
            self.loop.run_until_complete(_upgrade.callback(self.context, building_class.name))

            with self.assertRaises(BuildingNotBuilt):
                kingdom.get_building(building_class)
