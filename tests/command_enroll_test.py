from test import DiscordSTRTestCase
from commands.enroll import enroll as _enroll
from units.minion import Minion
from units.soldier import Soldier
from buildings.barracks import Barracks
from common import *


class CommandEnrollTestCase(DiscordSTRTestCase):
    def setUp(self):
        """Initialise a basic Kingdom"""
        super(CommandEnrollTestCase, self).setUp()
        
        self.generate_kingdom()
    
    def test_enroll_minion(self):
        """Enrolling one minion"""
        kingdom = self.get_kingdom()
        kingdom._ressources = Minion.cost
        
        self.loop.run_until_complete(_enroll.callback(self.context, Minion.name, 1))

        self.assertEqual(kingdom.units.get_unit(Minion).training, 1)

        self.end_tasks()

        self.assertEqual(kingdom.units.get_unit(Minion).ready, 1)
        self.assertEqual(kingdom.units.get_unit(Minion).training, 0)
        self.assertEqual(kingdom.units.get_unit(Minion).total, 1)
    
    def _test_enroll_unit(self, unit_class):
        """Enrolling one unit"""
        kingdom = self.get_kingdom()
        kingdom._ressources = unit_class.cost
        
        self.loop.run_until_complete(_enroll.callback(self.context, unit_class.name, 1))

        self.assertEqual(kingdom.units.get_unit(unit_class).training, 1)

        self.end_tasks()

        self.assertEqual(kingdom.units.get_unit(unit_class).ready, 1)
        self.assertEqual(kingdom.units.get_unit(unit_class).training, 0)
        self.assertEqual(kingdom.units.get_unit(unit_class).total, 1)
    
    def _test_enroll_unit_max_population_reached(self, unit_class):
        """Enrolling unit when the kingdom is full or is about to be"""
        kingdom = self.get_kingdom()
        kingdom._ressources = unit_class.cost * 1000
        
        if unit_class != Minion:
            self.infinite_workforce()
        
        max_unit = kingdom.max_unit_storage(unit_class)

        # Reach max population
        self.loop.run_until_complete(_enroll.callback(self.context, unit_class.name, max_unit))

        self.assertEqual(kingdom.units.get_unit(unit_class).training, max_unit)

        # Trying to enroll a unit when the max populaton is reached
        self.loop.run_until_complete(_enroll.callback(self.context, unit_class.name, 1))

        self.assertEqual(kingdom.units.get_unit(unit_class).total, max_unit)
        
        self.end_tasks()

        self.assertEqual(kingdom.units.get_unit(unit_class).ready, max_unit)
        self.assertEqual(kingdom.units.get_unit(unit_class).training, 0)
        self.assertEqual(kingdom.units.get_unit(unit_class).total, max_unit)
    
    def _test_enroll_unit_no_money(self, unit_class):
        """Trying to enroll unit when we don't have the money for it"""
        kingdom = self.get_kingdom()
        
        max_unit = kingdom.max_unit_storage(unit_class)
        
        kingdom._ressources = unit_class.cost * (max_unit - 1)

        self.loop.run_until_complete(_enroll.callback(self.context, unit_class.name, max_unit))
        
        with self.assertRaises(OutOfUnit):
            kingdom.units.get_unit(unit_class)
        
        self.loop.run_until_complete(_enroll.callback(self.context, unit_class.name, 1))
        
        self.loop.run_until_complete(_enroll.callback(self.context, unit_class.name, max_unit - 1))

        self.assertEqual(kingdom.units.get_unit(unit_class).total, 1)
    
    def test_enroll_soldier_building_requierements_not_met(self):
        """Enrolling one soldier when you don't have any barracks"""
        kingdom = self.get_kingdom()
        
        self.large_workforce()
        self.large_ressources()
        
        Soldier.required_building = [Barracks]

        self.loop.run_until_complete(_enroll.callback(self.context, Soldier.name, 1))
        
        with self.assertRaises(OutOfUnit):
            kingdom.units.get_unit(Soldier)
    
    def test_enroll_soldier_no_minion(self):
        """Enrolling one soldier when you don't have any minion to train"""
        kingdom = self.get_kingdom()
        
        self.large_ressources()
        self.build_all_buildings()

        self.loop.run_until_complete(_enroll.callback(self.context, Soldier.name, 1))
        
        with self.assertRaises(OutOfUnit):
            kingdom.units.get_unit(Soldier)
    
    def test_enroll_unit(self):
        """Enrolling one unit"""
        for unit_class in l_units:
            if unit_class != Minion:
                self.large_workforce()
                self.build_all_buildings()
            
            self._test_enroll_unit(unit_class)
    
    def test_enroll_unit_max_population_reached(self):
        """Enrolling unit when the kingdom is full or is about to be"""
        for unit_class in l_units:
            if unit_class != Minion:
                self.large_workforce()
                self.build_all_buildings()
            
            self._test_enroll_unit(unit_class)
    
    def test_enroll_unit_no_money(self):
        """Trying to enroll unit when we don't have the money for it"""
        for unit_class in l_units:
            if unit_class != Minion:
                self.large_workforce()
                self.build_all_buildings()
            
            self._test_enroll_unit(unit_class)