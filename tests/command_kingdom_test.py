from classes.ressources import Ressources
from test import DiscordSTRTestCase
from commands.kingdom import kingdom as _kingdom
from common import *


class CommandKingdomTestCase(DiscordSTRTestCase):
    def test_kingdom_creation(self):
        """Claiming your kingdom"""
        self.loop.run_until_complete(_kingdom.callback(self.context, "Jean de la roulette russe"))

        kingdom = self.get_kingdom()
        self.assertEqual(kingdom.name, "Jean de la roulette russe")
        self.assertEqual(len(kingdom.units), 0)
        self.assertEqual(len(kingdom.squads), 0)
        self.assertEqual(kingdom.ressources, Ressources(100, 0, 0, 0))

    def test_no_kingdom(self):
        """Trying to get a kingdom while you have none"""
        with self.assertRaises(UserHasNoKingdom):
            kingdom = self.get_kingdom()

    def test_command_kingdom_no_params(self):
        """Saying "?kingdom" and test if you have a kingdom"""
        with self.assertRaises(UserHasNoKingdom):
            self.loop.run_until_complete(_kingdom.callback(self.context))
            
            kingdom = self.get_kingdom()
    
    def test_renaming_kingdom(self):
        """Saying "?kingdom <name>" a second time"""
        self.loop.run_until_complete(_kingdom.callback(self.context, "Nom 1"))
        self.loop.run_until_complete(_kingdom.callback(self.context, "Nom 2"))

        self.assertEqual(self.get_kingdom().name, "Nom 1")
