import bot
from classes.exception import *
from test import DiscordSTRTestCase
from commands.squad import squad as _squad
from commands.enroll import enroll as _enroll
from commands.cheat import _cheat
from units.soldier import Soldier


class CommandSquadTestCase(DiscordSTRTestCase):
    def setUp(self):
        """Initialise a basic Kingdom"""
        super(CommandSquadTestCase, self).setUp()
        self.generate_kingdom()

    def test_formation_no_parameters(self):
        try:
            self.loop.run_until_complete(_squad.callback(self.context))
        except Exception:
            self.fail("Exception raised on command ?squad")

    def test_formation_with_name_but_no_squad(self):
        with self.assertRaises(SquadNotFound):
            self.loop.run_until_complete(_squad.callback(self.context, "test"))

    def test_formation_create_with_no_name(self):
        with self.assertRaises(ForbiddenSquadName):
            self.loop.run_until_complete(_squad.callback(self.context, "create"))

    def test_formation_create_with_name(self):
        self.assertEqual(len(self.get_kingdom().squads.l_squads), 0)
        self.loop.run_until_complete(_squad.callback(self.context, "create", "test 1"))
        self.assertEqual(len(self.get_kingdom().squads.l_squads), 1)
        self.loop.run_until_complete(_squad.callback(self.context, "create", "test 2"))
        self.assertEqual(len(self.get_kingdom().squads.l_squads), 2)
        squad = self.get_kingdom().squads.get_by_name("test 1")
        self.assertEqual(len(squad.l_units), 0)
        squad = self.get_kingdom().squads.get_by_name("test 2")
        self.assertEqual(len(squad.l_units), 0)

    def test_assign_no_name_unit_no_amount(self):
        with self.assertRaises(NoSquadNameGiven):
            self.loop.run_until_complete(_squad.callback(self.context, "assign"))
        with self.assertRaises(NoAssignDetails):
            self.loop.run_until_complete(_squad.callback(self.context, "assign", "test 12"))

    def test_assign_correct_name_unit_no_amount(self):
        with self.assertRaises(BadQuantityGiven):
            self.loop.run_until_complete(_squad.callback(self.context, "assign", "test 12", "soldier"))

    def test_assign_unknown_squad(self):
        with self.assertRaises(SquadNotFound):
            self.loop.run_until_complete(_squad.callback(self.context, "assign", "test 12", "soldier", 1))

    def test_assign_known_squad_not_enough_units(self):
        self.loop.run_until_complete(_squad.callback(self.context, "create", "test 1"))
        with self.assertRaises(NotEnoughUnitsAvailable):
            self.loop.run_until_complete(_squad.callback(self.context, "assign", "test 1", "soldier", 1))

    def test_assign_existant_squad_enough_units(self):
        #Create a squad
        self.loop.run_until_complete(_squad.callback(self.context, "create", "test 1"))

        #enroling 2 soldier
        self.loop.run_until_complete(_cheat.callback(self.context, "enroll", "soldier", 2))

        #assign 1 soldier to the squad
        self.loop.run_until_complete(_squad.callback(self.context, "assign", "test 1", "soldier", 1))

        kingdom = self.get_kingdom()
        self.assertEqual(len(kingdom.squads.get_by_name("test 1")), 1)
        self.assertEqual(kingdom.units.available_units(Soldier.name), 1)
        self.loop.run_until_complete(_squad.callback(self.context, "assign", "test 1", "soldier", 1))
        self.assertEqual(len(kingdom.squads.get_by_name("test 1")), 2)
        self.assertEqual(kingdom.units.available_units(Soldier.name), 0)
        with self.assertRaises(NotEnoughUnitsAvailable):
            self.loop.run_until_complete(_squad.callback(self.context, "assign", "test 1", "soldier", 1))

    def test_revoke_squad_no_params(self):
        with self.assertRaises(NoSquadNameGiven):
            self.loop.run_until_complete(_squad.callback(self.context, "revoke"))

    def test_revoke_unknown_squad(self):
        with self.assertRaises(SquadNotFound):
            self.loop.run_until_complete(_squad.callback(self.context, "revoke", "lolilol", "soldier", 1))

    def test_revoke_not_enough_unit(self):
        #Create a squad
        self.loop.run_until_complete(_squad.callback(self.context, "create", "test 1"))

        #enroling 2 soldier
        self.loop.run_until_complete(_cheat.callback(self.context, "enroll", "soldier", 2))

        with self.assertRaises(NotUnitSquaded):
            self.loop.run_until_complete(_squad.callback(self.context, "revoke", "test 1", "soldier", 1))

        #assign 1 soldier to the squad
        self.loop.run_until_complete(_squad.callback(self.context, "assign", "test 1", "soldier", 1))
        with self.assertRaises(NotEnoughUnitsSquaded):
            self.loop.run_until_complete(_squad.callback(self.context, "revoke", "test 1", "soldier", 2))
        kingdom = self.get_kingdom()
        self.assertEqual(len(kingdom.squads.get_by_name("test 1")), 1)

    def test_revoke_correct_unit(self):
        self.loop.run_until_complete(_squad.callback(self.context, "create", "test 1"))
        self.loop.run_until_complete(_cheat.callback(self.context, "enroll", "soldier", 2))
        self.loop.run_until_complete(_squad.callback(self.context, "assign", "test 1", "soldier", 1))
        kingdom = self.get_kingdom()
        self.assertEqual(len(kingdom.squads.get_by_name("test 1")), 1)
        self.loop.run_until_complete(_squad.callback(self.context, "revoke", "test 1", "soldier", 1))
        self.assertEqual(len(kingdom.squads.get_by_name("test 1")), 0)

    def test_dismantle_no_params(self):
        with self.assertRaises(NoSquadNameGiven):
            self.loop.run_until_complete(_squad.callback(self.context, "dismantle"))

    def test_dismantle_unknown_squad(self):
        with self.assertRaises(SquadNotFound):
            self.loop.run_until_complete(_squad.callback(self.context, "dismantle", "lolilol"))

    def test_dismantle_squad_with_no_soldier(self):
        self.loop.run_until_complete(_squad.callback(self.context, "create", "test 1"))
        kingdom = self.get_kingdom()
        self.assertEqual(len(kingdom.squads.get_by_name("test 1")), 0)
        self.get_kingdom().squads.get_by_name("test 1")
        self.loop.run_until_complete(_squad.callback(self.context, "dismantle", "test 1"))
        with self.assertRaises(SquadNotFound):
            self.get_kingdom().squads.get_by_name("test 1")

    def test_dismantle_squad_with_soldier(self):
        self.loop.run_until_complete(_squad.callback(self.context, "create", "test 1"))
        self.loop.run_until_complete(_cheat.callback(self.context, "enroll", "soldier", 2))
        self.loop.run_until_complete(_squad.callback(self.context, "assign", "test 1", "soldier", 1))
        kingdom = self.get_kingdom()
        self.assertEqual(len(kingdom.squads.get_by_name("test 1")), 1)
        self.assertEqual(kingdom.units.available_units(Soldier.name), 1)
        self.get_kingdom().squads.get_by_name("test 1")
        self.loop.run_until_complete(_squad.callback(self.context, "dismantle", "test 1"))
        with self.assertRaises(SquadNotFound):
            self.get_kingdom().squads.get_by_name("test 1")
        self.assertEqual(kingdom.units.available_units(Soldier.name), 2)





