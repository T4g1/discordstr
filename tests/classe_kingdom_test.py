from classes.kingdom import Kingdom
from classes.ressources import Ressources
from common import get_unit_class
from test import DiscordSTRTestCase
from units.minion import Minion


class ClassKingdomTestCase(DiscordSTRTestCase):
    def create_kingdom(self):
        return Kingdom("test", 50, 50)
    

    def test_instantiate_kingdom(self):
        try:
            self.create_kingdom()
        except:
            self.fail("Création de kingdom fail")

    def test_kingdom_property_workforce(self):
        kingdom = self.create_kingdom()
        self.assertEqual(kingdom.workforce, 0)
        kingdom.add_workforce(10)
        self.assertEqual(kingdom.workforce, 10)

    def test_kingdom_property_ressources(self):
        kingdom = self.create_kingdom()
        self.assertEqual(kingdom.ressources, Ressources(100, 0, 0, 0))

    def test_kingdom_property_max_gold(self):
        kingdom = self.create_kingdom()
        self.assertEqual(kingdom.max_gold, kingdom.max_storage("gold"))

    def test_kingdom_property_max_food(self):
        kingdom = self.create_kingdom()
        self.assertEqual(kingdom.max_food, kingdom.max_storage("food"))

    def test_kingdom_property_max_wood(self):
        kingdom = self.create_kingdom()
        self.assertEqual(kingdom.max_wood, kingdom.max_storage("wood"))

    def test_kingdom_property_max_stone(self):
        kingdom = self.create_kingdom()
        self.assertEqual(kingdom.max_stone, kingdom.max_storage("stone"))

    def test_get_total_workforce(self):
        kingdom = self.create_kingdom()
        self.assertEqual(kingdom.get_total_workforce(), 0)
        kingdom.add_workforce(10)
        self.assertEqual(kingdom.get_total_workforce(), 10)

    def test_pay(self):
        kingdom = self.create_kingdom()
        kingdom.pay(Ressources(gold=50))
        self.assertEqual(kingdom.ressources, Ressources(50, 0, 0, 0))

    def test_can_afford(self):
        kingdom = self.create_kingdom()
        self.assertEqual(kingdom.can_afford(Ressources(gold=50)), True)
        self.assertEqual(kingdom.can_afford(Ressources(gold=500)), False)

    def test_get_lack_of(self):
        kingdom = self.create_kingdom()
        a = kingdom.get_lack_of(Ressources(gold=500))
        self.assertIsNone(kingdom.get_lack_of(Ressources(gold=50)))
        self.assertEqual(kingdom.get_lack_of(Ressources(gold=500)), [400, 0, 0, 0])

    def test_max_affordable(self):
        kingdom = self.create_kingdom()
        self.assertEqual(kingdom.max_affordable(Ressources(gold=50)), 2)

    def test_max_storage(self):
        kingdom = self.create_kingdom()
        self.assertEqual(kingdom.max_storage(Ressources(50)), 500)

    def test_max_assignation(self):
        kingdom = self.create_kingdom()
        self.assertEqual(kingdom.max_assignation(), Ressources(5, 5, 5, 5))

    def test_max_unit_storage(self):
        kingdom = self.create_kingdom()
        self.assertEqual(kingdom.max_unit_storage(get_unit_class(Minion.name)), 20)







