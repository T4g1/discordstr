from test import DiscordSTRTestCase
from classes.ressources import Ressources
from commands.farming import mine as _mine, farm as _farm, chop as _chop, quarry as _quarry
from common import *


class CommandFarmingTestCase(DiscordSTRTestCase):
    def setUp(self):
        """Initialise a basic Kingdom"""
        super(CommandFarmingTestCase, self).setUp()
        
        self.generate_kingdom()
    
    def test_mine(self):
        """Send minions to the mine"""
        kingdom = self.get_kingdom()
        self.large_workforce()

        max_assigned = kingdom.max_assignation().gold

        # Send one
        self.loop.run_until_complete(_mine.callback(self.context))
        self.assertEqual(kingdom.assigned, Ressources(gold=1))

        # Max assignable
        self.loop.run_until_complete(_mine.callback(self.context, max_assigned - 1))
        self.assertEqual(kingdom.assigned, Ressources(gold=max_assigned))
        
        # More than max assignable
        self.loop.run_until_complete(_mine.callback(self.context))
        self.assertEqual(kingdom.assigned, Ressources(gold=max_assigned))
        
        # Recall too much
        self.loop.run_until_complete(_mine.callback(self.context, max_assigned + 1, "recall"))
        self.assertEqual(kingdom.assigned, Ressources(gold=max_assigned))
        
        # Recall one
        self.loop.run_until_complete(_mine.callback(self.context, 1, "recall"))
        self.assertEqual(kingdom.assigned, Ressources(gold=max_assigned - 1))
        
        # Recall all
        self.loop.run_until_complete(_mine.callback(self.context, kingdom.assigned.gold, "recall"))
        self.assertEqual(kingdom.assigned, Ressources(gold=0))
    
    def test_quarry(self):
        """Send minions to the quarry"""
        kingdom = self.get_kingdom()
        self.large_workforce()

        max_assigned = kingdom.max_assignation().stone

        # Send one
        self.loop.run_until_complete(_quarry.callback(self.context))
        self.assertEqual(kingdom.assigned, Ressources(stone=1))

        # Max assignable
        self.loop.run_until_complete(_quarry.callback(self.context, max_assigned - 1))
        self.assertEqual(kingdom.assigned, Ressources(stone=max_assigned))
        
        # More than max assignable
        self.loop.run_until_complete(_quarry.callback(self.context))
        self.assertEqual(kingdom.assigned, Ressources(stone=max_assigned))
        
        # Recall too much
        self.loop.run_until_complete(_quarry.callback(self.context, max_assigned + 1, "recall"))
        self.assertEqual(kingdom.assigned, Ressources(stone=max_assigned))
        
        # Recall one
        self.loop.run_until_complete(_quarry.callback(self.context, 1, "recall"))
        self.assertEqual(kingdom.assigned, Ressources(stone=max_assigned - 1))
        
        # Recall all
        self.loop.run_until_complete(_quarry.callback(self.context, kingdom.assigned.stone, "recall"))
        self.assertEqual(kingdom.assigned, Ressources(stone=0))
    
    def test_chop(self):
        """Send minions to the woods"""
        kingdom = self.get_kingdom()
        self.large_workforce()

        max_assigned = kingdom.max_assignation().wood

        # Send one
        self.loop.run_until_complete(_chop.callback(self.context))
        self.assertEqual(kingdom.assigned, Ressources(wood=1))

        # Max assignable
        self.loop.run_until_complete(_chop.callback(self.context, max_assigned - 1))
        self.assertEqual(kingdom.assigned, Ressources(wood=max_assigned))
        
        # More than max assignable
        self.loop.run_until_complete(_chop.callback(self.context))
        self.assertEqual(kingdom.assigned, Ressources(wood=max_assigned))
        
        # Recall too much
        self.loop.run_until_complete(_chop.callback(self.context, max_assigned + 1, "recall"))
        self.assertEqual(kingdom.assigned, Ressources(wood=max_assigned))
        
        # Recall one
        self.loop.run_until_complete(_chop.callback(self.context, 1, "recall"))
        self.assertEqual(kingdom.assigned, Ressources(wood=max_assigned - 1))
        
        # Recall all
        self.loop.run_until_complete(_chop.callback(self.context, kingdom.assigned.wood, "recall"))
        self.assertEqual(kingdom.assigned, Ressources(wood=0))
    
    def test_farm(self):
        """Send minions to the woods"""
        kingdom = self.get_kingdom()
        self.large_workforce()

        max_assigned = kingdom.max_assignation().food
        
        # Send one
        self.loop.run_until_complete(_farm.callback(self.context))
        self.assertEqual(kingdom.assigned, Ressources(food=1))
        
        # Max assignable
        self.loop.run_until_complete(_farm.callback(self.context, max_assigned - 1))
        self.assertEqual(kingdom.assigned, Ressources(food=max_assigned))
        
        # More than max assignable
        self.loop.run_until_complete(_farm.callback(self.context))
        self.assertEqual(kingdom.assigned, Ressources(food=max_assigned))
        
        # Recall too much
        self.loop.run_until_complete(_farm.callback(self.context, max_assigned + 1, "recall"))
        self.assertEqual(kingdom.assigned, Ressources(food=max_assigned))
        
        # Recall one
        self.loop.run_until_complete(_farm.callback(self.context, 1, "recall"))
        self.assertEqual(kingdom.assigned, Ressources(food=max_assigned - 1))
        
        # Recall all
        self.loop.run_until_complete(_farm.callback(self.context, kingdom.assigned.food, "recall"))
        self.assertEqual(kingdom.assigned, Ressources(food=0))
