from test import DiscordSTRTestCase
from classes.ressources import Ressources


class ClassRessourcesTestCase(DiscordSTRTestCase):
    def test_ressources_operators(self):
        """Test all operators of Ressources classe"""
        a = Ressources(100, 50, 25, 0) * 3
        self.assertEqual(Ressources(300, 150, 75, 0), a)

        a = Ressources(100, 50, 25, 0) * Ressources(1, -1, 10, 1250)
        self.assertEqual(Ressources(100, -50, 250, 0), a)
        
        a = Ressources(100, 50, 25, 0) + 100
        self.assertEqual(Ressources(200, 150, 125, 100), a)
        
        a = Ressources(100, 50, 25, 0) + Ressources(100, 50, 25, 0)
        self.assertEqual(Ressources(100, 50, 25, 0) * 2, a)

        a = Ressources(100, 50, 25, 0) * 2
        b = Ressources(100, 50, 25, 0) + Ressources(100, 50, 25, 0)
        c = a - Ressources(100, 50, 25, 0)
        c *= 2

        self.assertEquals(a, b, c)

        a = Ressources(100, 100, 100, 100)
        b = Ressources(5, 5, 5, 5)

        self.assertEqual(a > b, True)
        self.assertEqual(b < a, True)
        
        c = Ressources(100, 99, 100, 100)

        self.assertEqual(c <= a, True)
        self.assertEqual(c < a, False)
        
        self.assertEqual(a >= c, True)
        self.assertEqual(a > c, False)

        self.assertEqual(a == a, True)
        self.assertEqual(a != c, True)