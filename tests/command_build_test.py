from test import DiscordSTRTestCase
from commands.build import build as _build
from classes.ressources import Ressources
from common import *


class CommandBuildTestCase(DiscordSTRTestCase):
    def setUp(self):
        """Initialise a basic Kingdom"""
        super(CommandBuildTestCase, self).setUp()
        
        self.generate_kingdom()
    
    def test_build(self):
        """Try to build every buildings with requirements met"""
        kingdom = self.get_kingdom()
        
        for building_class in l_buildings:
            kingdom._ressources = building_class.cost
            self.large_workforce()

            self.loop.run_until_complete(_build.callback(self.context, building_class.name))

            self.assertEqual(kingdom.get_building(building_class).is_built(), False)
            
            self.end_tasks()
            
            self.assertEqual(kingdom.get_building(building_class).is_built(), True)
    
    def test_build_without_ressources(self):
        """Try to build  without ressources"""
        kingdom = self.get_kingdom()
        
        for building_class in l_buildings:
            # Don't do that test for buildings that built themself without workforce (wtf ?)
            if building_class.builders_needed <= 0:
                pass
            
            kingdom._ressources = Ressources()
            self.large_workforce()

            self.loop.run_until_complete(_build.callback(self.context, building_class.name))

            with self.assertRaises(BuildingNotBuilt):
                kingdom.get_building(building_class)
    
    def test_build_without_workforce(self):
        """Try to build without minions to build it"""
        kingdom = self.get_kingdom()
        
        for building_class in l_buildings:
            kingdom._ressources = building_class.cost

            self.loop.run_until_complete(_build.callback(self.context, building_class.name))

            with self.assertRaises(BuildingNotBuilt):
                kingdom.get_building(building_class)
