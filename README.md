# DiscordSTR #

## Getting started ##

* Clone the repository
* Install requirements
```
#!
pip install -r requirements.txt
```
* ?????
* PROFIT

## Workflow ##

* Create your own branch for the modifications you want to do (never work on master)

In order for your work to be merged or, more generaly, for a branche to be merged, it must meet some requirements:

* All test cases in the latest master version must pass. Test with:
```
#!
python test.py
```
* You must have implemented custom test for your new features (if any)
* Document all of your work, if something is not clear, we will not merge

When all of those requirements are met, contact the repository owner to check your work and merge it with master.


## Quick overview ##

### Commands ###

Used to store all commands known to the bot

### Units/Buildings ###

Contains all existing units. Each added unit/building must be added to the units/buildings list in the **globals.py** file

### Classes ###

Contains all classes used for the bot

* **Building**
* **Config**: Manages the bot configuration
* **Kingdom**: All data of a kingdom as well as a lot of methods to manage it
* **Ressources**: Describe a ressource (one field per goods) and mathematical operations over it
* **Task**: Describe a genereic task that can last for a given time before completion
* **Unit**
* **User**: Gives informations about the user

### Misc ###

* **common.py**: All function that doesn't fits anywhere else as well as units and buildings listing
* **config.ini**: Bot's configuration
* **data.pickle**: Saved data (users and kingdoms)
* **bot.py**: Stores the bot declaration
* **test.py**: Base file for tests