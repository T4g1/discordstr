import pickle
from discord.ext import commands
from classes.worldmap import WorldMap


description = '''DiscordSTR allow you to play a text-based STR in Discord ! Type ?help to get started'''
bot = commands.Bot(command_prefix='?', description=description, help_attrs={'name': None})


# Allows us to define a DEBUGGING mode with no Discord output
bot._send_message = bot.send_message


async def send_message(destination, content, *args, **kwargs):
    """Send a message to a given destination"""
    if not getattr(bot, "STR_debugging", False):
        await bot._send_message(destination, content, *args, **kwargs)
    
    if getattr(bot, "STR_verbose", False):
        print(content)


def set_debugging(debugging, verbose=False):
    """Activate debugging or deactivates it"""
    setattr(bot, "STR_debugging", debugging)
    setattr(bot, "STR_verbose", verbose)


def init_data():
    """Clear all bot's data"""
    bot.data = {}
    bot.data['users'] = {}
    bot.data['worldmap'] = WorldMap(max_player=500)


def load_data():
    """Load all data of the bot from data.pickle file"""
    try:
        with open('data.pickle', 'rb') as data_file:
            data = pickle.load(data_file)
            
            bot.data = data
            
            if not "users" in data:
                bot.data['users'] = {}

            if not "worldmap" in data:
                bot.data['worldmap'] = WorldMap(max_player=500)
        
        print("Data loaded, found {0} user(s).".format(len(bot.data['users'])))
    except:
        bot.init_data()
        
        print("No data found, default values used.")


def save_data():
    """Save all data of the bot"""
    with open('data.pickle', 'wb') as out_file:
        data = bot.data

        pickle.dump(data, out_file)
    
    print("Data saved.")


bot.send_message = send_message
bot.set_debugging = set_debugging
bot.init_data = init_data
bot.load_data = load_data
bot.save_data = save_data