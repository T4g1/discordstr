import math

from classes.config import options
from common import *
from classes.exception import OutOfUnit


class UnitsManager:
    def __init__(self, name=""):
        self.name = name
        self.l_units = []

    @property
    def max_unit(self):
        if self.name != "":
            return options["DETAILS"]["max_unit_by_squad"]
        else:
            return math.inf

    def get_unit(self, unit_class):
        """Get an unit by its class name"""
        for unit in self.l_units:
            if unit.__class__ == unit_class:
                return unit

        raise OutOfUnit(unit_class)

    def available_units(self, name):
        """Gives the amount of unit available"""
        unit_class = get_unit_class(name)
        
        try:
            unit = self.get_unit(unit_class)
            
            return unit.ready
        except OutOfUnit:
            return 0

    def add_unit(self, to_add):
        """Add units to the list of units"""
        if len(self) + to_add.total > self.max_unit:
            raise NotEnougSquadRoom(self.name)
        
        try:
            unit = self.get_unit(type(to_add))
            
            unit.ready += to_add.ready
            unit.training += to_add.training
        except OutOfUnit:
            self.l_units.append(to_add)

    def remove_unit(self, to_remove):
        """Removes amount unit from the squad"""
        unit = self.get_unit(type(to_remove))
        
        if unit.ready >= to_remove.ready and unit.training >= to_remove.training:
            unit.ready -= to_remove.ready
            unit.training -= to_remove.training
        else:
            raise NotEnoughUnits(self.name, type(to_remove).name, to_remove.total)

    def __len__(self):
        """Gives the size of the squad"""
        count = 0
        for unit in self.l_units:
            count += unit.total
        
        return int(count)

    def __iter__(self):
        """Iterate through the unit list"""
        yield from self.l_units

    def __str__(self):
        return self.name

    def __repr__ (self):
        return self.__str__()

    def append(self, value):
        self.l_units.append(value)
