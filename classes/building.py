from enum import Enum
from classes.exception import *
from classes.ressources import Ressources


class Building:
    """Base class for every building"""
    required_building = []
    upgrade_costs = []
    cost = Ressources()
    
    def __init__(self):
        self.status = Status.BUILDING
        self.level = 1
        self.maxlevel = 5
    
    def is_built(self):
        return self.status != Status.BUILDING
    
    def is_upgrading(self):
        return self.status != Status.UPGRADING
    
    def finish_construction(self):
        """Finishes the building process"""
        if self.status != Status.BUILDING:
            raise BuildingNotInConstruction(type(self))
        
        self.status = Status.STABLE
    
    def finish_upgrade(self):
        """Finishes the current upgrade"""
        if self.status != Status.UPGRADING:
            raise BuildingIsNotUpgrading(type(self))
        
        self.upgrade()
        
        self.status = Status.STABLE
    
    def start_upgrade(self):
        """Set status to UPGRADING"""
        if self.status == Status.UPGRADING:
            raise BuildingAlreadyUpgrading(type(self))
        
        self.status = Status.UPGRADING

    def upgrade(self):
        self.level = min(self.level + 1, self.maxlevel)
    
    def get_upgrade_time(self):
        """Time needed to upgrade that building"""
        return 60

    def get_upgrade_cost(self):
        """Get the cost of the next upgrade"""
        if len(self.upgrade_costs) >= self.level:
            return self.cost * self.level
        else:
            return self.upgrade_costs[self.level - 1]

    def get_unit_storage(self):
        return 0
    
    def to_str(level=0):
        """Give a string explaining this building's benefits by level"""
        message = "Not built\n"
        if level > 0:
            message = "Level {level}\n".format(level=level)
        
        return message


class Status(Enum):
    STABLE = 0
    BUILDING = 1
    UPGRADING = 2
