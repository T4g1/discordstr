class User:
    """Contains all informations about an user"""
    
    titles = {
        "Chef": 0,
        "Mayor": 100,
        "Duc": 5000,
        "King": 10000,
        "Emperor": 10000000
    }
    
    def __init__(self, author):
        self.id = author.id
        self.name = author.name
        self.mention = author.mention
        
        self.showed_name_kingdom = False
        
        self.kingdom = None
    
    @property
    def title(self):
        user_title = "Paysan"
        title_required_workforce = -1
    
        if self.kingdom is None:
            return "Paysan"
        else:
            workforce = self.kingdom.get_total_workforce()
            
            for title, required_workforce in User.titles.items():
                if workforce >= required_workforce and title_required_workforce < required_workforce:
                    user_title = title
                    title_required_workforce = required_workforce
        
        return user_title