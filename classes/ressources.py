from classes.exception import ForbiddenRessourceMultiplication, ForbiddenRessourceAddition


class Ressources:
    """Describe an amount of ressources"""
    
    def __init__(self, gold=0, food=0, wood=0, stone=0):
        self.gold = gold
        self.food = food
        self.wood = wood
        self.stone = stone
    
    def __ge__(self, other):
        """Operator >="""
        return self.gold >= other.gold and self.food >= other.food and self.wood >= other.wood and self.stone >= other.stone
    
    def __le__(self, other):
        """Operator <="""
        return self.gold <= other.gold and self.food <= other.food and self.wood <= other.wood and self.stone <= other.stone
    
    def __gt__(self, other):
        """Operator >"""
        return self.gold > other.gold and self.food > other.food and self.wood > other.wood and self.stone > other.stone
    
    def __lt__(self, other):
        """Operator <"""
        return self.gold < other.gold and self.food < other.food and self.wood < other.wood and self.stone < other.stone
    
    def __eq__(self, other):
        """Operator =="""
        return self.gold == other.gold and self.food == other.food and self.wood == other.wood and self.stone == other.stone
    
    def __ne__(self, other):
        """Operator !="""
        return not self.__eq__(other)
    
    def __mul__(self, other):
        try:
            if type(other) == type(self):
                return Ressources(
                    gold=self.gold * other.gold, 
                    food=self.food * other.food, 
                    wood=self.wood * other.wood, 
                    stone=self.stone * other.stone)
            else:
                return Ressources(
                        gold=self.gold * other, 
                        food=self.food * other, 
                        wood=self.wood * other, 
                        stone=self.stone * other)
        except:
            raise ForbiddenRessourceMultiplication(other)
    
    def __rmul__(self, other):
        return self.__mul__(other)

    def __add__(self, other):
        """Operator+"""
        try:
            if type(other) == type(self):
                return Ressources(
                    gold = self.gold + other.gold,
                    food = self.food + other.food,
                    wood = self.wood + other.wood,
                    stone = self.stone + other.stone)
            else:
                return Ressources(
                    gold = self.gold + other,
                    food = self.food + other,
                    wood = self.wood + other,
                    stone = self.stone + other)
        except:
            raise ForbiddenRessourceAddition

    def __iadd__(self, other):
        """Operator+="""
        return self.__add__(other)

    def __sub__(self, other):
        """Operator-"""
        return self.__add__(other * (-1))

    def __isub__(self, other):
        """Operator-="""
        return self.__sub__(other)
    
    def __str__(self):
        """used to print the object"""
        return "[Gold: {self.gold}, Food: {self.food}, Wood: {self.wood}, Stone: {self.stone}]".format(self=self)
