from classes.exception import *

class WorldMap:
    def __init__(self, max_player=500, width=100, height=100, owned_positions=[]):
        self.width = width
        self.height = height
        self.owned_positions = owned_positions
        self.max_player = min(max_player, self.width * self.height)

    def get_position(self, x, y):
        """ Get a position by its coordinates """
        
        # Check if the given position isn't outside the map
        if x >= self.width or y >=self.height:
            raise WorldMapPositionOutOfRange(x, y)

        # Check if the position is already owned by someone else
        for position in self.owned_positions:
            if position['x'] == x and position['y'] == y:
                return position
            
        return None

    def add_position(self, owner, x, y):
        """ Allow to add a new owned position in the current map"""
        
        # check if the map if full
        if self.isfull():
            raise WorldMapIsFull()

        # check if the position is owned by someone (also check if the position isn't outside the map)
        if self.get_position(x, y) is not None:
            raise WorldMapPositionAlreadyOwned(x, y)

        # add the position
        self.owned_positions.append({'owner': owner, 'x': x, 'y': y})

    def isfull(self):
        """ Return True if the map is full"""
        
        return len(self.owned_positions) == self.max_player

    def __str__(self):
        """ Transform the class into a string"""
        
        owned_positions, owned_title = "", "This is an empty map"
        for position in self.owned_positions:        
            owned_positions += "\t{owner}: x: {x}; y: {y}\n".format(owner= position['owner'], x=position['x'],y=position['y'])
        if owned_positions != "":
            owned_title = "Owned Position:\n"
        return "Maximum allowed players: {max_player}\tMap Size:\twidth: {width}; height: {height}\n{owned_title}{owned_positions}".format(max_player=self.max_player, width=self.width, height=self.height, owned_title=owned_title, owned_positions=owned_positions)
                
