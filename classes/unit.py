from classes.exception import *


class Unit:
    def __init__(self, amount=0):
        self.ready = amount
        self.training = 0
    
    @property
    def total(self):
        return self.ready + self.training
    
    def train(self, quantity):
        """Adds units for training"""
        self.train += quantity
    
    def finish_training(self, quantity):
        """Remove units from training"""
        if self.training < quantity:
            raise NotEnoughUnitsInTraining(self, quantity)
        
        self.training -= quantity
        self.ready += quantity


# TODO: Merge with class Unit
class UnitStat:
    def __init__(self, attack_physical=0, attack_magical=0, attack_speed=0, resistance_physical=0, resistance_magical=0, healing=0, travel_speed=0):
        
        self.attack_physical = attack_physical
        self.attack_magical = attack_magical

        self.attack_speed = attack_speed

        self.resistance_physical = resistance_physical
        self.resistance_magical = resistance_magical

        self.healing = healing

        self.travel_speed = travel_speed
