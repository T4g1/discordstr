from classes.exception import *


class SquadsManager:
    """Describe a list of formation of unit"""
    def __init__(self):
        self.l_squads = []

    def check_squad(self, name):
        """Raise an error if the squad name doesn't exist"""
        if name not in [item.name for item in self.l_squads]:
            raise SquadNotFound(name)

    def get_by_name(self, name):
        """Gets a squad by its name"""
        if name == "":
            raise NoSquadNameGiven
        
        for squad in self.l_squads:
            if squad.name == name:
                return squad
        
        raise SquadNotFound(name)

    def units_squaded(self, squad_name, unit_class):
        """How many of the given unit are in the squad named name"""
        squad = self.get_by_name(squad_name)
        try:
            unit = squad.get_unit(unit_class)
        except:
            raise NotUnitSquaded
        
        return unit.ready

    def add_squad(self, squad):
        """Adds a squad in the list"""
        if squad.name == "":
            raise ForbiddenSquadName(squad.name)
        
        try:
            self.get_by_name(squad.name)
        
            raise SquadNameAlreadyTaken(squad.name)
        except SquadNotFound:
            self.l_squads.append(squad)

    def add_unit(self, squad_name, to_add):
        """Assign the given unit to the squad named squad_name"""
        for squad in self.l_squads:
            if squad.name == squad_name:
                squad.add_unit(to_add)

    def remove_unit(self, squad_name, to_remove):
        """Remove the given unit from the squad named squad_name"""
        for squad in self.l_squads:
            if squad.name == squad_name:
                squad.remove_unit(to_remove)

    def dismantle(self, squad_name):
        """Delete a squad using its name"""
        for squad in self.l_squads:
            if squad.name == squad_name:
                self.l_squads.remove(squad)
                return
        
        raise SquadNotFound(squad_name)

    def __iter__(self):
        """Iterate through the squads list"""
        yield from self.l_squads

    def __len__(self):
        return len(self.l_squads)
