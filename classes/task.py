import math
import time
import asyncio
import importlib


class Task:
    """Contain an ongoing task"""
    
    def __init__(self, user_id, channel_id, started_at, duration, workforce, module_path, func_name, desc, **kwargs):
        """
        
        :param user_id: ID of the user
        :param channel_id: Channel in which we issued the task
        :param started_at: Time of task start
        :param duration: Duration of the task
        :param workforce: Workforce needed for the task (removed at the beginning and restored on task deletion)
        :param module_path: Module in which we have the functiun
        :param func_name: Functiun in that module to call
        :param desc: Nice message to display describing the task
        :param kwargs: For all other tasks need
        """
        self.user_id = user_id
        self.channel_id = channel_id
        
        self.started_at = started_at
        self.duration = duration
        
        self.workforce = workforce
        
        self.module_path = module_path
        self.func_name = func_name
        
        self.desc = desc
        
        self.sleeping_process = None
        
        self.kwargs = kwargs
    
    def start(self):
        """Starts the task"""
        try:
            module = importlib.import_module(self.module_path)
            func = getattr(module, self.func_name)
            
            asyncio.ensure_future(func(self))
            
            #print("Started task: {task.module_path}.{task.func_name} {task.duration}s".format(task=self))
            return True
        except Exception as e:
            #print("Unknown task: {task.module_path}.{task.func_name}".format(task=self))
            return False
    
    async def sleep(self, delay):
        """Blocking call to wait a given delay of second"""
        coro = asyncio.sleep(delay)
        self.sleeping_process = asyncio.ensure_future(coro)
        
        try:
            return await self.sleeping_process
        except asyncio.CancelledError:
            pass
    
    def cancel(self):
        """Cancel the sleeping process"""
        if self.sleeping_process:
            self.sleeping_process.cancel()
        
        self.sleeping_process = None
    
    def __getstate__(self):
        """Say what should be saved in with pickle"""
        state = self.__dict__.copy()
        
        # Remove the sleeping_process field.
        del state['sleeping_process']
        
        return state

    def __setstate__(self, state):
        """Restore sleeping_process when loading from file"""
        self.__dict__.update(state)
        
        elapsed_time = time.time() - self.started_at
        time_left = max(0, math.ceil(self.duration - elapsed_time))
        
        self.started_at = time.time()
        self.duration = time_left
        
        self.start()
