import math
import time
from common import *
from units.minion import Minion
from classes.ressources import Ressources
from classes.unitsmanager import UnitsManager
from classes.squadsmanager import SquadsManager


class Kingdom:
    """Contains all informations about a player's kingdom"""

    default_storage = 500
    default_assignation = 5
    
    def __init__(self, name, x, y):
        self.name = name
        
        self.assigned = Ressources()                # Who's assigned to each ressource
        self._ressources = Ressources(gold = 100)   # How many ressoruce we have
        
        # Gain per ticks for each ressources per minion
        self._benefits = Ressources(
            gold = 0.1,
            food = 0.05,
            wood = 0.06,
            stone = 0.02
        )
        
        self._lastcheck = time.time()               # Last ressource check
        
        self.x = x
        self.y = y
        
        self.tasks = []
        self.buildings = []
        
        self.units = UnitsManager()
        self.squads = SquadsManager()
    
    @property
    def workforce(self):
        try:
            unit = self.units.get_unit(Minion)
            
            return unit.ready
        except OutOfUnit:
            return 0
    
    @property
    def ressources(self):
        now = time.time()
        elapsed_time = now - self._lastcheck
        
        self._ressources += (self._benefits * self.assigned)
        
        self._lastcheck = now
    
        return self._ressources
    
    @property
    def max_gold(self):
        return self.max_storage("gold")
    
    @property
    def max_food(self):
        return self.max_storage("food")
    
    @property
    def max_wood(self):
        return self.max_storage("wood")
    
    @property
    def max_stone(self):
        return self.max_storage("stone")
    
    def get_total_workforce(self):
        """Gets the real workforce, including minions doing tasks"""
        real_workforce = self.workforce
        
        for task in self.tasks:
            real_workforce += task.workforce
        
        return real_workforce
    
    def pay(self, cost: Ressources):
        """Pay the given cost
        
        Raise Exception if cost is higher than kingdom ressources
        """
        if self.can_afford(cost):
            self.ressources.gold -= cost.gold
            self.ressources.food -= cost.food
            self.ressources.wood -= cost.wood
            self.ressources.stone -= cost.stone
        else:
            raise Exception("Can't afford the given cost", cost)
    
    def can_afford(self, cost: Ressources):
        """Check that the costs are inferior or equals to kingdom's ressources"""
        return self.ressources >= cost

    def get_lack_of(self, cost: Ressources):
        gold = cost.gold - self.ressources.gold
        food = cost.food - self.ressources.food
        wood = cost.wood -self.ressources.wood
        stone = cost.stone - self.ressources.stone

        lack_of = [
            gold if gold > 0 else 0,
            food if food > 0 else 0,
            wood if wood > 0 else 0,
            stone if stone > 0 else 0,
        ]

        return lack_of if lack_of != [0, 0, 0, 0] else None
    
    def max_affordable(self, cost: Ressources):
        """Says how many the kingdom can buy at the given cost"""
        max_affordable = [math.inf]
        
        if cost.gold > 0:
            max_affordable.append(math.floor(self.ressources.gold / cost.gold))
        
        if cost.food > 0:
            max_affordable.append(math.floor(self.ressources.food / cost.food))
        
        if cost.wood > 0:
            max_affordable.append(math.floor(self.ressources.wood / cost.wood))
        
        if cost.stone > 0:
            max_affordable.append(math.floor(self.ressources.stone / cost.stone))
        
        return min(max_affordable)

    def max_storage(self, ressource):
        """Give the maximum capacity for a given ressource"""
        ressource_to_building = {
            "gold": "Treasury",
            "food": "Granary",
            "wood": "Warehouse",
            "stone": "Warehouse"
        }
        
        for building in self.buildings:
            if building.__class__.name == ressource_to_building[ressource]:
                return building.get_storage()
        
        return Kingdom.default_storage
    
    def max_assignation(self):
        """Get all maximal assignations of minions"""
        return Ressources(
            gold = self.max_assignation("gold"),
            food = self.max_assignation("food"),
            wood = self.max_assignation("wood"),
            stone = self.max_assignation("stone")
        )
    
    def max_assignation(self, ressource=""):
        """Says how many minions can work on the given ressource at the same time"""
        ressource_to_building = {
            "gold": "Mine",
            "food": "Farm",
            "wood": "Sawmill",
            "stone": "Quarry"
        }
        
        if ressource == "":
            return Ressources(
                gold=self.max_assignation("gold"),
                food=self.max_assignation("food"),
                wood=self.max_assignation("wood"),
                stone=self.max_assignation("stone")
            )
        
        for building in self.buildings:
            if building.__class__.name == ressource_to_building[ressource]:
                return building.__class__.max_minions(building.level)
        
        return Kingdom.default_assignation
    
    def max_unit_storage(self, unit_class):
        """ Give the maximun capacity for a given unit class"""
        try:
            building = self.get_building(unit_class.housing_building)
            
            return building.get_unit_storage()
        except BuildingNotBuilt:
            return unit_class.default_housing
    
    def assign_minion(self, amount, assignation):
        """Assign minions to a task"""
        self.remove_workforce(amount)
        
        self.assigned += assignation * amount
    
    def recall_minion(self, amount, assignation):
        """Remove minions from their assignation"""
        self.assigned -= assignation * amount
        
        self.add_workforce(amount)
        

    def add_ressources(self, ressources):
        """Add ressources to the kingdom and returns what have been gained"""
        gained = Ressources(
            gold = ressources.gold,
            food = ressources.food,
            wood = ressources.wood,
            stone = ressources.stone)
        
        # Check what will be gained
        if self.ressources.gold + ressources.gold > self.max_gold:
            gained.gold = self.max_gold - self.ressources.gold
        
        if self.ressources.food + ressources.food > self.max_food:
            gained.food = self.max_food - self.ressources.food
        
        if self.ressources.wood + ressources.wood > self.max_wood:
            gained.wood = self.max_wood - self.ressources.wood
        
        if self.ressources.stone + ressources.stone > self.max_stone:
            gained.stone = self.max_stone - self.ressources.stone
        
        # Add ressources
        self.ressources += ressources
        
        # Limit with max stock possible
        self.ressources = Ressources(
                gold = min(self.ressources.gold, self.max_gold),
                food = min(self.ressources.food, self.max_food),
                wood = min(self.ressources.wood, self.max_wood),
                stone = min(self.ressources.stone, self.max_stone))
        
        return gained
    
    def own_building(self, building_class):
        """Returns True if we own that building"""
        try:
            self.get_building(building_class)
        except:
            return False
            
        return True
    
    def get_building(self, building_class):
        """Gets a building by its class name"""
        for building in self.buildings:
            if building.__class__ == building_class:
                return building
        
        raise BuildingNotBuilt(building_class)
    
    def start_task(self, task):
        """Add a new task to the kingdom"""
        if task.workforce > 0:
            self.remove_workforce(task.workforce)
        
        self.tasks.append(task)
        
        return task.start()
    
    def remove_task(self, task):
        """Removve a task"""
        if task not in self.tasks:
            raise Exception("Task not found")
        
        if task.workforce > 0:
            self.add_workforce(task.workforce)
        
        self.tasks.remove(task)
    
    # TODO: Put this in UnitsManager
    def add_workforce(self, amount):
        """Adds minion to the kingdom"""
        try:
            unit = self.units.get_unit(Minion)
        
            unit.ready += amount
        except OutOfUnit:
            self.units.append(Minion(amount))
    
    # TODO: Put this in UnitsManager
    def remove_workforce(self, amount):
        """Removes minions from the kingdom"""
        if amount == 0:
            return
        
        try:
            unit = self.units.get_unit(Minion)

            if unit.ready < amount:
                raise NotEnoughWorkforce(unit.ready, amount)
            
            unit.ready -= amount
        except OutOfUnit:
            raise NotEnoughWorkforce(0, amount)
