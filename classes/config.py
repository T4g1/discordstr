import configparser as ConfigParser
import os
from os.path import dirname

import watchdog as watchdog
from watchdog.observers import Observer

global options

class Options(watchdog.events.FileSystemEventHandler):
    """watchdog permet de voir quand on modifie le fichier config"""
    # CONFIG_FILE = '../config.ini'
    MAIN_DIRECTORY = dirname(dirname(__file__))
    CONFIG_FILE = os.path.abspath(os.path.join(MAIN_DIRECTORY, 'config.ini'))
    REQUIRED_SECTIONS = (
        'credentials'
    )

    def __init__(self, *args, **kwargs):
        self._config = ConfigParser.RawConfigParser()
        self.reload_config()
        self._config_valid = False
        self.observer = Observer()
        self.observer.schedule(self, path='.', recursive=False)
        self.observer.start()

    def __del__(self):
        self.observer.stop()
        self.observer.join()

    def on_any_event(self, event):
        if self.CONFIG_FILE in event.src_path:
            self.reload_config()

    def reload_config(self):
        self._config.read(self.CONFIG_FILE)
        self.check_config()

    def check_config(self):
        self._config_valid = False

        try:
            for section in self.REQUIRED_SECTIONS:
                self._config.items(section)
            self._config_valid = True
        except ConfigParser.NoSectionError:
            self._config_valid = False

    def __getitem__(self, section):
        #TODO faire ça plus propre
        a = self._config.items(section)
        elm_to_conv = []
        for elm in a:
            try:
                int(elm[1])
                elm_to_conv.append(elm[0])
            except (Exception) as e:
                pass
        b = dict(a)
        for elm in elm_to_conv:
            b[elm] = int(b[elm])

        return b

    @property
    def valid(self):
        return self._is_config_valid

options = Options()
