class DiscordSTRException(Exception):
    def __init__(self):
        pass

    def __str__(self):
        print(type(self).__name__ + ": Oh noes, error :(")
        return "Oh noes, error :("


class UserHasNoKingdom(DiscordSTRException):
    def __init__(self, author):
        self.author = author

    def __str__(self):
        return "You don't have any kingdom to rule... Build one by typing `?kingdom`"


class NotPositiveInteger(DiscordSTRException):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return "The quantity must be greater than zero !"


class NotEnoughWorkforce(DiscordSTRException):
    def __init__(self, has, needed):
        self.has = has
        self.needed = needed
    
    def __str__(self):
        message = "You only have {has} minion(s) available for that task !".format(has=self.has)
        if self.has == 0:
            message = "You dont have any minion available for that task ! Type `?enroll` to enroll some."
        
        return "{message} (you need {needed} of them !)".format(message=message, needed=self.needed)


class BuildingNotBuilt(DiscordSTRException):
    """Building not built"""
    def __init__(self, building_class):
        self.building_class = building_class
    
    def __str__(self):
        return "You don't own a {name}, build one first with `?build {name}`".format(name=self.building_class.name)


class BuildingNotInConstruction(DiscordSTRException):
    """Building was said to finish a construction and he was not in cosntruction"""
    def __init__(self, building_class):
        self.building_class = building_class
    
    def __str__(self):
        return "You tried to finalize a construction on the building {name} but no construction was in progress !".format(name=self.building_class.name)


class BuildingAlreadyUpgrading(DiscordSTRException):
    """Building is already being upgraded"""
    def __init__(self, building_class):
        self.building_class = building_class
    
    def __str__(self):
        return "The building {name} is already upgrading...".format(name=self.building_class.name)


class BuildingIsNotUpgrading(DiscordSTRException):
    """Building was said to finish an upgrade that never happened"""
    def __init__(self, building_class):
        self.building_class = building_class
    
    def __str__(self):
        return "You tried to finalize an upgrade on the building {name} but no upgrade where started !".format(name=self.building_class.name)


class OutOfUnit(DiscordSTRException):
    """We dont have that unit anymore (or we never had that unit)"""
    def __init__(self, unit_class):
        self.unit_class = unit_class


class UnknwonBuilding(DiscordSTRException):
    """Building name doesnt exists"""
    def __init__(self, building_name):
        self.building_name = building_name

    def __str__(self):
        if self.building_name == "":
            return "You have to specify a building name !"
        
        return "The building '{building_name}' does not exist".format(building_name=self.building_name)


class UnknwonUnit(DiscordSTRException):
    """Unit name doesnt exists"""
    def __init__(self, unit_name):
        self.unit_name = unit_name

    def __str__(self):
        if self.unit_name == "":
            return "You have to specify an unit name !"
        
        return "The unit {unit_name} does not exist".format(unit_name=self.unit_name)


class BuildingsRequieremntsNotMet(DiscordSTRException):
    """Unit name doesnt exists"""
    def __init__(self, l_buildings):
        self.l_buildings = l_buildings

    def __str__(self):
        message = ""
        for building in self.l_buildings:
            message += "{name}, ".format(name=building.name)
        
        if message != "":
            message = message[:-2]
        
        return "You must build the following buildings to do that: {message}".format(message=message)


class WorldMapPositionOutOfRange(DiscordSTRException):
    def __init__(self, x, y):
        pass


class WorldMapPositionAlreadyOwned(DiscordSTRException):
    def __init__(self, x, y):
        pass


class WorldMapIsFull(DiscordSTRException):
    def __init__(self):
        pass


class MaxPopulationReached(DiscordSTRException):
    def __init__(self, unit_class, max, has, wanted):
        self.unit_class = unit_class
        self.max = max
        self.has = has
        self.wanted = wanted
    
    def __str__(self):
        return "You can't enroll {wanted} {unit}, your max. population for this unit is {max} and you already have {has} of them... You can re-try with only {max_wanted}".format(max=self.max, unit=self.unit_class.name, wanted=self.wanted, has=self.has, max_wanted=self.max - self.has)


class NotEnoughUnitsInTraining(DiscordSTRException):
    def __init__(self, unit, quantity):
        self.unit = unit
        self.quantity = quantity

    def __str__(self):
        return "You only have {training} {name} in training and you want to finish the training of {quantity}".format(training=self.unit.training, name=type(self.unit).name, quantity=self.quantity)


class NotEnoughUnits(DiscordSTRException):
    def __init__(self, squad_name, unit_name, quantity):
        self.squad_name = squad_name
        self.unit_name = unit_name
        self.quantity = quantity

    def __str__(self):
        return "You don't have {amount} {unit_name} in the squad {squad_name}".format(squad_name=self.squad_name, unit_name=self.unit_name, quantity=self.quantity)


class NotEnougSquadRoom(DiscordSTRException):
    def __init__(self, name):
        self.name = name

    def __str__(self):
        return "You don't have enough space in the squad '{name}' for this many units !".format(name=self.name)


class SquadNameAlreadyTaken(DiscordSTRException):
    def __init__(self, name):
        self.name = name

    def __str__(self):
        return "Squad name '{name}' is already taken".format(name=self.name)


class SquadNotFound(DiscordSTRException):
    def __init__(self, name):
        self.name = name

    def __str__(self):
        return "Squad name '{name}' was not found".format(name=self.name)

class ForbiddenSquadName(DiscordSTRException):
    def __init__(self, name):
        self.name = name

    def __str__(self):
        return "Squad name '{name}' is forbidden".format(name=self.name)

class BadQuantityGiven(DiscordSTRException):
    def __init__(self):
        pass

    def __str__(self):
        return "Bad quantity given."

class NoAssignDetails(DiscordSTRException):
    def __init__(self):
        pass

    def __str__(self):
        return "no unit name or quantity given"

class NotEnoughUnitsAvailable(DiscordSTRException):
    def __init__(self):
        pass

    def __str__(self):
        return "not enough units available"

class NoSquadNameGiven(DiscordSTRException):
    def __init__(self):
        pass

    def __str__(self):
        return "no squad name given"

class NotEnoughUnitsSquaded(DiscordSTRException):
    def __init__(self):
        pass

    def __str__(self):
        return "not enough units in this squad"

class NotUnitSquaded(DiscordSTRException):
    def __init__(self):
        pass

    def __str__(self):
        return "no units in this squad"

class ForbiddenRessourceMultiplication(DiscordSTRException):
    def __init__(self, other):
        self.other = other
    
    def __str__(self):
        return "Trying to multiply a Ressources with a non-integer and non-Ressources element"

class ForbiddenRessourceAddition(DiscordSTRException):
    def __init__(self, other):
        self.other = other
    
    def __str__(self):
        return "Trying to add a Ressources with a non-integer and non-Ressources element"
