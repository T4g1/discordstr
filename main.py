import asyncio
import logging
import atexit
from common import *
from classes.config import options
from commands import *

logging.basicConfig(level=logging.INFO)


@bot.event
async def on_ready():
    """Loads previously created kingdoms and game settings from data.pickle."""
    
    print('Logged in as: {}, ID: {}'.format(bot.user.name, bot.user.id))
    
    await bot.change_status(game=discord.Game(name="Type ?kingdom to create your kingdom !"))
    
    bot.load_data()


async def on_command_error(event, *args, **kwargs):
    pass

#bot.add_listener(on_command_error, 'on_command_error')


@atexit.register
def on_exit():
    """handle the bot exit"""
    bot.save_data()


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    
    try:
        key = options['CREDENTIALS']['key']
        loop.run_until_complete(bot.start(key))
    except KeyboardInterrupt:
        loop.run_until_complete(bot.logout())
    finally:
        loop.close()
