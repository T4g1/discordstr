from classes.ressources import Ressources
from classes.building import Building


class Farm(Building):
    """Allows more minions to work on the food simultaneously"""
    
    name = "Farm"
    cost = Ressources(gold=50, food=50, wood=50, stone=50)
    upgrade_costs = [
        Ressources(gold=1000, food=200, wood=500, stone=1000),
        Ressources(gold=1500, food=300, wood=750, stone=1500),
        Ressources(gold=2500, food=500, wood=1250, stone=2500),
        Ressources(gold=5000, food=1000, wood=2500, stone=5000)
    ]
    build_time = 60
    builders_needed = 15
    
    def __init__(self):
        Building.__init__(self)
        self.maxlevel = 5
    
    def max_minions(level):
        """Max minions by level"""
        return (level + 1) * 5
    
    def to_str(level=0):
        """Give a string explaining this building's benefits by level"""
        message = Building.to_str(level)
        message += "Max. minions on food: {storage}".format(storage=Farm.max_minions(level))
        
        return message
