from classes.ressources import Ressources
from classes.building import Building


class Granary(Building):
    """Allow you to store more food in your kingdom"""

    MAXSTORAGEBYLEVEL = [500, 1000, 2500, 4000, 6000, 10000]
    name = "Granary"
    cost = Ressources(gold=50, food=50, wood=50, stone=50)
    upgrade_costs = [
        Ressources(gold=1000, food=200, wood=500, stone=1000),
        Ressources(gold=1500, food=300, wood=750, stone=1500),
        Ressources(gold=2500, food=500, wood=1250, stone=2500),
        Ressources(gold=5000, food=1000, wood=2500, stone=5000)
    ]
    build_time = 60
    builders_needed = 15
    
    def __init__(self):
        Building.__init__(self)
        self.maxlevel = 5

    def get_storage(self):
        return Granary.MAXSTORAGEBYLEVEL[self.level]
    
    def to_str(level=0):
        """Give a string explaining this building's benefits by level"""
        message = Building.to_str(level)
        message += "Max. food: {storage}".format(storage=Granary.MAXSTORAGEBYLEVEL[level])
        
        return message
