from classes.ressources import Ressources
from classes.building import Building


class Warehouse(Building):
    """Allow you to store more wood and stone in your kingdom"""

    MAXSTORAGEBYLEVEL = [500, 1000, 2500, 4000, 6000, 10000]
    name = "Warehouse"
    cost = Ressources(gold=100, food=20, wood=50, stone=100)
    upgrade_costs = [
        Ressources(gold=1000, food=200, wood=500, stone=1000),
        Ressources(gold=1500, food=300, wood=750, stone=1500),
        Ressources(gold=2500, food=500, wood=1250, stone=2500),
        Ressources(gold=5000, food=1000, wood=2500, stone=5000)
    ]
    build_time = 60
    builders_needed = 15
    
    def __init__(self):
        Building.__init__(self)
        self.maxlevel = 5

    def get_storage(self):
        return Warehouse.MAXSTORAGEBYLEVEL[self.level]
    
    def to_str(level=0):
        """Give a string explaining this building's benefits by level"""
        message = Building.to_str(level)
        message += "Max. wood and stone: {storage}".format(storage=Warehouse.MAXSTORAGEBYLEVEL[level])
        
        return message
