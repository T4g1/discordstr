from classes.ressources import Ressources
from classes.building import Building


class Barracks(Building):
    """Allow you to enroll some soldiers"""
    name = "Barracks"
    cost = Ressources(gold=100, food=20, wood=50, stone=100)
    upgrade_costs = [
        Ressources(gold=1000, food=200, wood=500, stone=1000),
        Ressources(gold=1500, food=300, wood=750, stone=1500),
        Ressources(gold=2500, food=500, wood=1250, stone=2500),
        Ressources(gold=5000, food=1000, wood=2500, stone=5000)
    ]
    build_time = 60
    builders_needed = 15

    def __init__(self):
        Building.__init__(self)
        self.maxlevel = 5

    def get_unit_storage(self):
        return 5
