from classes.ressources import Ressources
from classes.unit import Unit, UnitStat


class Heros(Unit):
    """ The heros of your kingdom"""
    
    cost = Ressources(gold=100, wood=50, stone=25)
    name = "Heros"
    stat = UnitStat(attack_physical=20, attack_speed=1, resistance_physical=10, resistance_magical=1)
    
    default_housing = 1
    housing_building = None

    required_building = []
    train_time = 15
    
    def __init__(self, amount=0):
        super(Heros, self).__init__(amount)
