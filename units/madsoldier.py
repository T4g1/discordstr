from classes.ressources import Ressources
from classes.unit import Unit, UnitStat
from buildings.granary import Granary
from buildings.barracks import Barracks



class MadSoldier(Unit):
    """ Better basic attackers"""
    
    cost = Ressources(gold=100, wood=50, stone=25)
    name = "MadSoldier"
    stat = UnitStat(attack_physical=20, attack_speed=1, resistance_physical=10, resistance_magical=1)
    
    default_housing = 0
    housing_building = Barracks
    
    required_building = [
        Barracks,
        Granary
    ]
    train_time = 15
    
    def __init__(self, amount=0):
        super(MadSoldier, self).__init__(amount)
