from classes.ressources import Ressources
from classes.unit import Unit, UnitStat


class Minion(Unit):
    """The basic citizen of your kingdom"""
    
    cost = Ressources(gold=100)
    name = "Minion"
    
    default_housing = 20
    housing_building = None
    
    required_building = []
    train_time = 15

    def __init__(self, amount=0):
        super(Minion, self).__init__(amount)
