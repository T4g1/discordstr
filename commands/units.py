from common import *


@bot.command(pass_context=True)
async def units(context):
    """Shows various informations about available unit.
    
    `?units` Shows all the available units in your kingdom
    """
    author = context.message.author
    users = bot.data['users']
    
    try:
        author = context.message.author
        users = bot.data['users']
        
        user, kingdom = kingdom_required(author, users)
    
        # Shows units's information
        workforce_symbol = "+" if kingdom.workforce > 0 else "-"
        total_workforce = kingdom.get_total_workforce()

        unit_message = ""
        for unit_class in l_units:
            try:
                unit = kingdom.units.get_unit(unit_class)

                training = unit.training
                amount = unit.ready
                symbol = "+"
            except OutOfUnit:
                training = 0
                amount = 0
                symbol = "-"
            
            max_unitamout_message = ""
            max_unitamout = kingdom.max_unit_storage(unit_class)
            if  max_unitamout != None:
                max_unitamout_message = "/{0}".format(max_unitamout)

            unit_message += "{symbol} {unit_class.name}: Available: {amount}{maxamout}\tTraining: {training}\t{unit_class.__doc__}\n".format(symbol=symbol, unit_class=unit_class, amount=amount, maxamout=max_unitamout_message, training=training)
        
        await bot.say(("```diff\n" +
                   "!===== [{user.title} {user.name}'s units] =====!\n" +
                   unit_message +
                   "!=========================================!" +
                   "```").format(user=user, kingdom=kingdom, workforce_symbol=workforce_symbol,total_workforce=total_workforce))
    except DiscordSTRException as e:
        await bot.say(e)
