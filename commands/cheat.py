from common import *


@bot.command(pass_context=True)
async def _cheat(context, command="", *args):
    """For developpers only !"""
    try:
        author = context.message.author
        users = bot.data['users']
        
        if command == 'whisper':
            #?cheat whisper Greenlamp "coucou lolilol"
            target = get_user(argument, context.message.channel.server.members)
            await send_whisper(target, args[1])
        elif command == 'user':
            await get_info_user(argument, context.message.channel.server.members)
        elif command == 'save':
            save_data()

            await bot.say("Data saved.")
        else:
            user, kingdom = kingdom_required(author, users)

            if command == 'gold' or command == 'COINAGE':
                kingdom.ressources.gold += int(args[0])
                await bot.say("{0} {1} was added to your kingdom".format(args[0], command))
            elif command == 'food' or command == 'PEPPERONI_PIZZA':
                kingdom.ressources.food += int(args[0])
                await bot.say("{0} {1} was added to your kingdom".format(args[0], command))
            elif command == 'wood' or command == 'WOODSTOCK':
                kingdom.ressources.wood += int(args[0])
                await bot.say("{0} {1} was added to your kingdom".format(args[0], command))
            elif command == 'stone' or command == 'QUARRY':
                kingdom.ressources.stone += int(args[0])
                await bot.say("{0} {1} was added to your kingdom".format(args[0], command))
            elif command == 'endall':
                await end_all_tasks(user, kingdom)
            elif command == 'enroll':
                type = args[0]
                qty = args[1]
                unit_class = get_unit_class(type)
                kingdom.units.add_unit(unit_class(int(qty)))
                await bot.say("A new {name} is awaiting for your orders !".format(name=unit_class.name))
            else:
                kingdom.ressources.gold += 100000
                kingdom.ressources.food += 100000
                kingdom.ressources.wood += 100000
                kingdom.ressources.stone += 100000
                await bot.say("100000 gold, 100000 food, 100000 wood and 100000 stone were added to your kingdom's stocks")
    except ValueError:
        await bot.say("Do you even think ?")
    except DiscordSTRException as e:
        await bot.say(e)

async def end_all_tasks(user, kingdom):
    for task in kingdom.tasks:
        task.cancel()
    
    await bot.say("All tasks ended")

async def send_whisper(user, message):
    await whisper(user, message)

async def get_info_user(name, members):
    try:
        await bot.say(get_user(name, members))
    except (Exception) as e:
        await bot.say(e)
