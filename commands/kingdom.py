from common import *
from classes.user import User
from classes.kingdom import Kingdom
from classes.ressources import Ressources
from units.minion import Minion


@bot.command(pass_context=True)
async def kingdom(context, name=""):
    """Shows various informations about your kingdom.
    
    `?kingdom` Shows all the available data about your kingdom
    `?kingdom <name>` Give a name to a new kingdom (you must be kingdom-less to do so)
    """
    author = context.message.author
    users = bot.data['users']
    worldmap = bot.data['worldmap']
    
    # New player
    if not author.id in users:
        users[author.id] = User(author)
    
    user = users[author.id]

    # We want to give a name for the kingdom
    if name is not "":
        if user.kingdom is None:
            try:
                x, y = add_new_random_map_position(worldmap, user.name)
            except WorldMapIsFull:
                await bot.say("The Map is full bro")
                return
            
            user.kingdom = Kingdom(name, x, y)
            
            await bot.say("{user.title} {user.name} claimed the {kingdom.name} kingdom !".format(user=user, kingdom=user.kingdom))
        else:
            await bot.say("Your kingdom is already known as the {kingdom.name} kingdom !".format(kingdom=user.kingdom))
            
    # The kingdom has no name
    elif user.kingdom is None:
        # We already ask to give a name for the kingdom
        if user.showed_name_kingdom:
            await bot.say("What are you waiting for {user.name} ? **Give your kingdom a name !** Do so by typing `?kingdom <the name you want>`".format(user=user))
        else:
            await bot.say("Greetings migthy {user.name}, I'm whathever you'll call me, your devoted minion ! Your first task as a ruler is to **give your kingdom a name !** Please, do so by typing `?kingdom <the name you want>`".format(user=user))
            
            user.showed_name_kingdom = True
    
        return
    
    kingdom = user.kingdom
    
    # Shows kingdom's information
    workforce_symbol = "+" if kingdom.workforce > 0 else "-"
    gold_symbol = "+" if kingdom.ressources.gold > 0 else "-"
    food_symbol = "+" if kingdom.ressources.food > 0 else "-"
    wood_symbol = "+" if kingdom.ressources.wood > 0 else "-"
    stone_symbol = "+" if kingdom.ressources.stone > 0 else "-"
    ongoing_tasks = "% No task in progress\n"
    
    # Count of ongoing tasks
    if len(kingdom.tasks) > 0:
        ongoing_tasks = "% Ongoing tasks: {0}\n".format(len(kingdom.tasks))
    
    total_workforce = kingdom.get_total_workforce()
    max_workforce = kingdom.max_unit_storage(Minion)

    max_assignation = Ressources(
        gold = kingdom.max_assignation("gold"),
        food = kingdom.max_assignation("food"),
        wood = kingdom.max_assignation("wood"),
        stone = kingdom.max_assignation("stone")
    )

    await bot.say(("```diff\n" +
        "!===== [{user.title} {user.name}'s Kingdom] =====!\n" +
        "+ Name: {kingdom.name}\n" +
        "+ Position: X: {kingdom.x}; Y: {kingdom.y}\n" +
        "{workforce_symbol} Minions: Available: {kingdom.workforce} Total: {total_workforce} Max: {max_workforce}\n" +
        "{gold_symbol} Gold: {kingdom.ressources.gold:,g}/{kingdom.max_gold}\tWorkers: {assigned.gold}/{max_assignation.gold}\n" +
        "{food_symbol} Food: {kingdom.ressources.food:,g}/{kingdom.max_food}\tWorkers: {assigned.food}/{max_assignation.food}\n" +
        "{wood_symbol} Wood: {kingdom.ressources.wood:,g}/{kingdom.max_wood}\tWorkers: {assigned.wood}/{max_assignation.wood}\n" +
        "{stone_symbol} Stone: {kingdom.ressources.stone:,g}/{kingdom.max_stone}\tWorkers: {assigned.stone}/{max_assignation.stone}\n" +
        ongoing_tasks +
        "!=========================================!" +
        "```").format(user=user, kingdom=kingdom, workforce_symbol=workforce_symbol, max_workforce=max_workforce, gold_symbol=gold_symbol, food_symbol=food_symbol, wood_symbol=wood_symbol, stone_symbol=stone_symbol, total_workforce=total_workforce, assigned=kingdom.assigned, max_assignation=kingdom.max_assignation()))
