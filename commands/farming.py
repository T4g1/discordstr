from common import *
from classes.ressources import Ressources


@bot.command(pass_context=True)
async def mine(context, workforce=1, recall=""):
    """Sends some minions farming to get gold.
    
    `?mine` to send one minion mining
    `?mine <quantity>`to send `<quantity>` minions to the mine
    `?mine <quantity> recall`to recall `<quantity>` minions from the mine
    """
    await _assign_minions(context.message.author, workforce, Ressources(gold=1), "Mine", recall)


@bot.command(pass_context=True)
async def farm(context, workforce=1, recall=""):
    """Sends some minions farming to get food.
    
    `?farm` to send one minion farming
    `?farm <quantity>`to send `<quantity>` minions to the farms
    `?farm <quantity> recall`to recall `<quantity>` minions from the farms
    """
    await _assign_minions(context.message.author, workforce, Ressources(food=1), "Farms", recall)


@bot.command(pass_context=True)
async def chop(context, workforce=1, recall=""):
    """Sends some minions farming to get wood.
    
    `?chop` to send one minion in the woods
    `?chop <quantity>`to send `<quantity>` minions to the woods
    `?chop <quantity> recall`to recall `<quantity>` minions from the woods
    """
    await _assign_minions(context.message.author, workforce, Ressources(wood=1), "Sawmill", recall)


@bot.command(pass_context=True)
async def quarry(context, workforce=1, recall=""):
    """Sends some minions farming to get stone.
    
    `?quarry` to send one minion in the quarry
    `?quarry <quantity>`to send `<quantity>` minions to the quarry
    `?quarry <quantity> recall`to recall `<quantity>` minions from the quarry
    """
    await _assign_minions(context.message.author, workforce, Ressources(stone=1), "Quarry", recall)


async def _assign_minions(author, workforce, assignation, place, recall):
    """Called by farm/chop/mine/quarry commands"""
    try:
        users = bot.data['users']
        
        if recall != "":
            if recall == "recall":
                user, kingdom = kingdom_required(author, users)
                
                if workforce <= 0:
                    raise NotPositiveInteger(workforce)
                
                if kingdom.assigned * assignation >= workforce * assignation:
                    kingdom.recall_minion(workforce, assignation)
                    
                    await bot.say("Recalled {workforce} minion(s) from the {place} !".format(workforce=workforce, place=place))
                else:
                    await bot.say("Not that much minions are assigned to the {place} !".format(place=place))
            else:
                await bot.say("To recall your minions, dont forget to put `recall` at the end of the command")
        else:
            user, kingdom = workforce_required(author, users, workforce)
            
            # Can't assign that much minions to that task !
            if not kingdom.max_assignation() >= kingdom.assigned + (workforce * assignation):
                await bot.say("You can't add {workforce} minion(s) to the {place}, upgrades it first to add more minions to it !".format(workforce=workforce, place=place))
            else:
                kingdom.assign_minion(workforce, assignation)
            
                await bot.say("Sent {workforce} minion(s) to the {place} !".format(workforce=workforce, place=place))
    except DiscordSTRException as e:
        await bot.say(e)