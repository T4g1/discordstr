import time
from common import *
from classes.task import Task


@bot.command(pass_context=True)
async def upgrade(context, name="", cost=""):
    """Upgrade a building.
    
    `?upgrade` to get a list of upgradable buildings
    `?upgrade <name>` to upgrade to building named `<name>`
    `?upgrade <name> cost` to get the costs for upgrading the building named `<name>`
    """
    try:
        author = context.message.author
        users = bot.data['users']
        
        user, kingdom = kingdom_required(author, users)
        
        ressources = kingdom.ressources
        
        if name != "":
            building_class = get_building_class(name)
            name = building_class.name
                
            building = kingdom.get_building(building_class)
            
            if cost != "":
                # Wants the cost of an upgrade
                if cost == "cost":
                    level = building.level
                    cost = building.get_upgrade_cost()
                    
                    symbol_gold = "+" if ressources.gold >= cost.gold else "-"
                    symbol_food = "+" if ressources.food >= cost.food else "-"
                    symbol_wood = "+" if ressources.wood >= cost.wood else "-"
                    symbol_stone = "+" if ressources.stone >= cost.stone else "-"
                
                    await bot.say(("```diff\n"
                            "! Cost to advance {building_class.name} to level {next_level}:\n"
                            "% {building_class.__doc__}\n"
                            "{symbol_gold} Gold: {ressources.gold:g}/{cost.gold}\n"
                            "{symbol_food} Food: {ressources.food:g}/{cost.food}\n"
                            "{symbol_wood} Wood: {ressources.wood:g}/{cost.wood}\n"
                            "{symbol_stone} Stone: {ressources.stone:g}/{cost.stone}\n"
                            "\n"
                            "- Currently: {current}\n"
                            "+ Next: {next}"
                            "```"
                            ).format(building_class=building_class, cost=cost, ressources=ressources, symbol_gold=symbol_gold, symbol_food=symbol_food, symbol_wood=symbol_wood, symbol_stone=symbol_stone, next_level=level + 1, current=building_class.to_str(level), next=building_class.to_str(level + 1)))
                else:
                    await bot.say("Please use `?upgrade <name> cost` to get the cost of the upgrade for the `<name>` building")
                    return
            else:
                # Can you upgrade it?
                cost = building.get_upgrade_cost()
                if not kingdom.can_afford(cost):
                    await bot.say("You can't afford this upgrade for your {name}".format(name=name))
                    return
                
                building.start_upgrade()
                kingdom.pay(cost)
                
                if not kingdom.start_task(Task(
                        user_id = user.id,
                        channel_id = context.message.channel.id,
                        started_at = time.time(),
                        duration = building.get_upgrade_time(),
                        workforce = building_class.builders_needed,
                        module_path = "commands.upgrade",
                        func_name = "upgrade_task",
                        desc = "Upgrading {name} to level {level}".format(name=name, level=building.level + 1),
                        building_name = name)):
                    # Unknown error, no task started
                    await bot.say("Sorry my {user.title}, we can't do that rigth now...".format(user=user))
                else:
                    await bot.say("You just began the updating of the {name} building".format(name=name))
        else:
            upgrade_message = ""
            for building_class in l_buildings:
                try:
                    building = kingdom.get_building(building_class)
                
                    cost = building.get_upgrade_cost()
                    
                    upgrade_message += "**{building_class.name}**\tCost for **level {next_level}**:\nGold: {cost.gold}, Food: {cost.food}, Wood: {cost.wood}, Stone: {cost.stone}\n".format(building_class=building_class, cost=cost, ressources=ressources, next_level=building.level + 1)
                except BuildingNotBuilt:
                    continue
            
            await bot.say("**Available upgrades:**\n" + upgrade_message)
    except DiscordSTRException as e:
        await bot.say(e)


async def upgrade_task(task):
    """Sleeps while upgrades are in progress"""
    try:
        await task.sleep(task.duration)
        
        channel = bot.get_channel(task.channel_id)
        
        users = bot.data['users']
        user = users[task.user_id]
        
        kingdom = user.kingdom
        if kingdom == None:
            return
        
        building_name = task.kwargs['building_name']
        building_class = get_building_class(building_name)
        
        building = kingdom.get_building(building_class)
        building.finish_upgrade()
        
        kingdom.remove_task(task)
        
        await bot.send_message(channel, "The building {name} has been upgraded to level {level} !".format(name=building_name, level=building.level))
    except BuildingNotBuilt:
        await bot.send_message(channel, "The building {name} was upgrading but it is not built yet ?!".format(name=building_name))
    except DiscordSTRException as e:
        await bot.say(e)
