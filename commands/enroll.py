import time
from common import *
from units.minion import Minion
from classes.task import Task


@bot.command(pass_context=True)
async def enroll(context, unit="minion", quantity=1):
    """Enroll minions to work in your kingdom.

    `?enroll` to enroll one minion
    `?enroll` <unit> to enroll one unit    
    `?enroll <unit> <quantity>` to enroll more than one unit at a time
    """
    try:
        author = context.message.author
        users = bot.data['users']
        
        user, kingdom = kingdom_required(author, users)
        
        unit_class = get_unit_class(unit)
        
        if quantity <= 0:
            raise NotPositiveInteger(quantity)
        
        check_buildings_requierements(kingdom, unit_class)

        # Get the amount of that unit possessed
        try:
            unit = kingdom.units.get_unit(unit_class)
            
            possessed = unit.total
        except OutOfUnit:
            possessed = 0
        
        # Get the maximun capacity for that unit type
        storage = kingdom.max_unit_storage(unit_class)
        
        # Quantity is maxed to capcity of that unit type
        if possessed + quantity > storage:
            raise MaxPopulationReached(unit_class, storage, possessed, quantity)
        
        # Calcul the cost and check if you can afford
        total_cost = quantity * unit_class.cost
        can_afford = kingdom.max_affordable(unit_class.cost)
            
        # In case you dont have enough cash !
        if can_afford < quantity:
            needed = total_cost - kingdom.ressources
            
            afford_status = "you can't afford enrolling any {name}s".format(name=unit_class.name)
            
            if can_afford > 0:
                afford_status = "you can only afford enrolling {can_afford:,} {name}(s)".format(can_afford=can_afford, name=unit_class.name)
            
            await bot.say("Gather more ressources before doing that, {afford_status} ! You need {needed.gold:,}G, {needed.food:,} wood, {needed.wood:,} food and {needed.stone:,} stone more...".format(afford_status=afford_status, needed=needed))
            return
        
        # You need to have minions that can be trained to do that
        if unit_class != Minion:
            if kingdom.workforce < quantity:
                raise NotEnoughWorkforce(kingdom.workforce, quantity)

            kingdom.remove_workforce(quantity)

        # Adds the unit
        try:
            unit = kingdom.units.get_unit(unit_class)
            unit.training += quantity
        except OutOfUnit:
            unit = unit_class()
            unit.training = quantity
            
            kingdom.units.add_unit(unit)
        
        # Pay
        kingdom.pay(total_cost)

        if not kingdom.start_task(Task(
                user_id=user.id,
                channel_id=context.message.channel.id,
                started_at=time.time(),
                duration=unit_class.train_time,
                workforce=0,
                quantity=quantity,
                module_path="commands.enroll",
                func_name="enroll_task",
                desc="Training {quantity} {name}".format(quantity=quantity, name=unit_class.name),
                unit_name=unit_class.name)):
            # Unknown error, no task started
            await bot.say("Sorry my {user.title}, we can't do that rigth now...".format(user=user))
            return
        
        if quantity == 1:
            await bot.say("A {name} has been sent to training !".format(name=unit_class.name))
        else:
            await bot.say("{quantity:,} new {name}s have been sent for training !".format(quantity=quantity, name=unit_class.name))
    except NotEnoughWorkforce as mc2:
        await bot.say("You're trying to train {e.needed} minions to {name} but you only have {e.has} minions...".format(e=mc2, name=unit_class.name))
    except DiscordSTRException as e:
        await bot.say(e)


async def enroll_task(task):
    """Sleeps while unit is training"""
    try:
        await task.sleep(task.duration)
        
        channel = bot.get_channel(task.channel_id)
        
        users = bot.data['users']
        user = users[task.user_id]
        
        quantity = task.kwargs['quantity']
        if quantity <= 0:
            raise Exception("Empty training task")

        unit_name = task.kwargs['unit_name']
        unit_class = get_unit_class(unit_name)
        
        kingdom = user.kingdom
        if kingdom == None:
            await bot.send_message(channel, "{workforce} {name}(s) have returned from their training, only to witness the total annihilation of its(their) home(s)...".format(name=unit_name, workforce=task.workforce))
            return
        
        # Get the amount of that unit ready
        try:
            unit = kingdom.units.get_unit(unit_class)

            ready = unit.ready
        except OutOfUnit:
            ready = 0

        # Get the maximun capacity for that unit type
        storage = kingdom.max_unit_storage(unit_class)

        quantity_trained = quantity
        if ready + quantity > storage:
            quantity_trained = storage - ready

        # Adds the unit
        try:
            unit = kingdom.units.get_unit(unit_class)
            unit.finish_training(quantity_trained)
        except OutOfUnit:
            unit = unit_class()
            unit.ready = quantity
            
            kingdom.units.add_unit(unit)
        
        kingdom.remove_task(task)

        if quantity_trained == 0:
            await bot.send_message(channel, "{quantity} {name} just finished his/their training but he/they had to go and find work in another realm as you don't have space left for them !".format(quantity=quantity, name=unit_class.name))
        elif quantity_trained == 1:
            await bot.send_message(channel, "A new {name} is awaiting for your orders !".format(name=unit_class.name))
        else:
            await bot.send_message(channel, "{quantity:,} new {name}s are awaiting for your orders !".format(quantity=quantity_trained, name=unit_class.name))
    except DiscordSTRException as e:
        await bot.send_message(channel, e)
