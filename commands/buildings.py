from common import *
from buildings import *


@bot.command(pass_context=True)
async def buildings(context, name=""):
    """List all available buildings.
    
    `?buildings` list all available buildings as well as the level of the ones already built in your kingdom.
    `?buildings <name>` gives information about the current benefits of a given building
    """
    try:
        author = context.message.author
        users = bot.data['users']
        
        user, kingdom = kingdom_required(author, users)
        
        # Informations for a particular building
        if name != "":
            building_class = get_building_class(name)
            name = building_class.name
            
            try:
                building = kingdom.get_building(building_class)
                
                level = building.level
            except BuildingNotBuilt:
                level = 0
                
            await bot.say(("```diff\n"
            "! {building_class.name}\n"
            "% {building_class.__doc__}\n"
            "- Currently: {current}\n"
            "+ Next: {next}"
            "```").format(building_class=building_class, current=building_class.to_str(level), next=building_class.to_str(level + 1)))
        
        # General listing
        else:
            buildings_message = ""
            for building_class in l_buildings:
                try:
                    building = kingdom.get_building(building_class)
                    
                    level = "(level {level})\t".format(level=building.level)
                    symbol = "+"
                except BuildingNotBuilt:
                    level = ""
                    symbol = "-"
                        
                buildings_message += "{symbol} {building_class.name}\t{level}{building_class.__doc__}\n".format(symbol=symbol, building_class=building_class, level=level)
        
            await bot.say(("```diff\n" +
                "!===== [{user.title} {user.name}'s buildings] =====!\n" +
                buildings_message +
                "!=========================================!" +
                "```").format(user=user))
    except DiscordSTRException as e:
        await bot.say(e)
