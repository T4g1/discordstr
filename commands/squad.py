from classes.unitsmanager import UnitsManager
from common import *


@bot.command(pass_context=True)
async def squad(context, task="", squad_name="", *args):
    """Manages your squads of units
    
    `?squad` show the list of your squads
    `?squad create <name>` create a squad named `<name>`
    `?squad assign <name> <unit> <quantity>` assigns the given quantity of unit to the squad named `<name>` if you have enough of that unit
    `?squad revoke <name> <unit> <quantity>` removes the given quantity of unit from the squad named `<name>`
    `?squad dismantle <name>` remove the squad named `<name>`
    """
    try:
        author = context.message.author
        users = bot.data['users']
        
        user, kingdom = kingdom_required(author, users)
        
        if task == "":
            await show_squad(context, user, kingdom)
        elif task == "create":
            await create_squad(context, user, kingdom, squad_name)
        elif task == "assign":
            if len(args) >= 1:
                unit_name = args[0]
                
                try:
                    amount = int(args[1])
                except:
                    raise BadQuantityGiven
                
                await enroll_unit(context, user, kingdom, squad_name, unit_name, amount)
            elif squad_name == "":
                raise NoSquadNameGiven
            else:
                raise NoAssignDetails
        elif task == "revoke":
            if len(args) >= 2:
                unit_name = args[0]
                
                try:
                    amount = int(args[1])
                except:
                    raise BadQuantityGiven
                
                await revoke_unit(context, user, kingdom, squad_name, unit_name, amount)
            elif squad_name == "":
                raise NoSquadNameGiven
            else:
                raise NoAssignDetails
        elif task == "dismantle":
            await dismantle_squad(context, user, kingdom, squad_name)
        elif task == "help":
            await bot.say("Command: create - assign - revoke - dismantle")
        else:
            await show_unit_in_squad(context, user, kingdom, squad_name=task)
    except DiscordSTRException as e:
        await bot.say(e)
        raise(e)


async def show_squad(context, user, kingdom):
    formation_message = ""
    for squad in kingdom.squads:
        size = len(squad)
        formation_message += "#{squad.name}: {current}/{max}\n".format(squad=squad, current=size, max=squad.max_unit)

    if formation_message == "":
        await bot.say("There's nos squad created.")
    else:
        await bot.say(("```diff\n" +
                       "!===== [{user.title} {user.name}'s squads] =====!\n" +
                       formation_message +
                       "!=========================================!" +
                       "```").format(user=user, formation_message=formation_message))


async def create_squad(context, user, kingdom, squad_name):
    kingdom.squads.add_squad(UnitsManager(squad_name))
    
    await bot.say("Squad {name} created with success.".format(name=squad_name))


async def enroll_unit(context, user, kingdom, squad_name, unit_name, amount):
    kingdom.squads.check_squad(squad_name)
    
    unit = get_unit_class(unit_name)
    
    available = kingdom.units.available_units(unit.name)
    if available >= amount:
        to_add = unit()
        to_add.ready = amount
        
        kingdom.units.remove_unit(to_add)
        kingdom.squads.add_unit(squad_name, to_add)
        await bot.say("{unit.name} assigned to the squad {squad_name}".format(unit=unit, squad_name=squad_name))
    else:
        raise NotEnoughUnitsAvailable


async def revoke_unit(context, user, kingdom, squad_name, unit_name, amount):
    kingdom.squads.check_squad(squad_name)
    
    unit = get_unit_class(unit_name)
    
    squaded = kingdom.squads.units_squaded(squad_name, unit)
    if amount <= squaded:
        to_remove = unit()
        to_remove.ready = amount
        
        kingdom.units.add_unit(to_remove)
        kingdom.squads.remove_unit(squad_name, to_remove)
        
        await bot.say("{amount}x {unit.name} revoked from the squad {squad_name}".format(amount=amount, unit=unit, squad_name=squad_name))
    else:
        raise NotEnoughUnitsSquaded


async def dismantle_squad(context, user, kingdom, squad_name):
    squad = kingdom.squads.get_by_name(squad_name)
    
    for unit in squad.l_units:
        kingdom.units.add_unit(unit)
    
    kingdom.squads.dismantle(squad_name)


async def show_unit_in_squad(context, user, kingdom, squad_name):
    squad = kingdom.squads.get_by_name(squad_name)

    await bot.say(squad.l_units)
