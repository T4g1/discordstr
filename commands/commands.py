from common import *


@bot.command()
async def commands(command=""):
    """List all available commands.
    
    `?commands` prints the list of all available commands.
    `?commands <command>` print the documentation of the given <command>
    """
    await _commands(command)

async def _commands(command=""):
    """Body of the commands function so this an be re-used for the help command."""
    # Commands list
    if command == "":
        command_list = "__Available commands:__\n"
        
        for name, command in sorted(bot.commands.items()):
            if name.startswith("_"):
                continue
            
            command_list += "**{name}**:\t{command.short_doc}\n".format(name=name, command=command)

        await bot.say(command_list + "More informations about a particular command with: `?commads <command>`")
    elif command in bot.commands and not command.startswith("_"):
        command = bot.commands[command]
        
        await bot.say("**{command.name}**\n{command.help}".format(command=command))
    else:
        await bot.say("That command does not exist")