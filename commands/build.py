import time
from classes.task import Task
from common import *


BUILD_TIME = 60

@bot.command(pass_context=True)
async def build(context, name="", cost=""):
    """Builds a structure
    
    `?build` to get a list of available buildings
    `?build <name>` to build the building called `<name>`
    `?build <name> cost` to get the costs for the building `<name>`
    """
    try:
        author = context.message.author
        users = bot.data['users']
        
        user, kingdom = kingdom_required(author, users)
        
        ressources = kingdom.ressources
        
        if name != "":
            building_class = get_building_class(name)
            name = building_class.name
                
            # Do you already own that building?
            if kingdom.own_building(building_class):
                await bot.say("You already have the {name} building. If you just want to upgrade it, use the following command: `?ugrade {name}`".format(name=name))
                return
            
            if cost != "":
                # Wants the cost of a building
                if cost == "cost":
                    cost = building_class.cost
                    
                    symbol_gold = "+" if ressources.gold >= cost.gold else "-"
                    symbol_food = "+" if ressources.food >= cost.food else "-"
                    symbol_wood = "+" if ressources.wood >= cost.wood else "-"
                    symbol_stone = "+" if ressources.stone >= cost.stone else "-"
                
                    await bot.say(("```diff\n"
                            "! {building_class.name}\n"
                            "% {building_class.__doc__}\n"
                            "{symbol_gold} Gold: {ressources.gold:g}/{cost.gold}\n"
                            "{symbol_food} Food: {ressources.food:g}/{cost.food}\n"
                            "{symbol_wood} Wood: {ressources.wood:g}/{cost.wood}\n"
                            "{symbol_stone} Stone: {ressources.stone:g}/{cost.stone}\n"
                            "```").format(building_class=building_class, cost=cost, ressources=ressources, symbol_gold=symbol_gold, symbol_food=symbol_food, symbol_wood=symbol_wood, symbol_stone=symbol_stone))
                else:
                    await bot.say("Please use `?build <name> cost` to get the cost of the `<name>` building")
                    return
            # Wants to build a building
            else:
                # Can you buy it?
                if not kingdom.can_afford(building_class.cost):
                    lack = kingdom.get_lack_of(building_class.cost)
                    lack_str = get_lack_str(lack)
                    await bot.say("You can't afford a {name}".format(name=name))
                    if lack_str is not None:
                        await bot.say("{lack_str}".format(lack_str=lack_str))
                    return
            
                # Do you have enough workforce to build it ?
                workforce_required(author, users, building_class.builders_needed)
                
                kingdom.pay(building_class.cost)
                kingdom.buildings.append(building_class())
                
                if not kingdom.start_task(Task(
                        user_id = user.id, 
                        channel_id = context.message.channel.id, 
                        started_at = time.time(), 
                        duration = building_class.build_time,
                        workforce = building_class.builders_needed,
                        module_path = "commands.build",
                        func_name = "build_task",
                        desc = "Building {name}".format(name=name),
                        building_name = name)):
                    # Unknown error, no task started
                    await bot.say("Sorry my {user.title}, we can't do that rigth now...".format(user=user))
                else:
                    await bot.say("You just began the construction of the {name} building".format(name=name))
        # Wants a listing of buildings
        else:
            build_message = ""
            for building_class in l_buildings:
                if kingdom.own_building(building_class):
                    continue
                
                build_message += "**{building_class.name}**\t{building_class.__doc__}\nGold: {cost.gold}, Food: {cost.food}, Wood: {cost.wood}, Stone: {cost.stone}\n".format(building_class=building_class, cost=building_class.cost, ressources=ressources)
        
            await bot.say("**Available to build:**\n" + build_message)
    except DiscordSTRException as e:
        await bot.say(e)


async def build_task(task):
    """Sleeps while builders are working"""
    try:
        await task.sleep(task.duration)
        
        channel = bot.get_channel(task.channel_id)
        
        users = bot.data['users']
        user = users[task.user_id]
        
        kingdom = user.kingdom
        if kingdom == None:
            await bot.send_message(channel, "{workforce} builder(s) have finished a building but there is nothing left of its(their) home(s)...".format(workforce=task.workforce))
            return
        
        building_name = task.kwargs['building_name']
        building_class = get_building_class(building_name)
        
        try:
            building = kingdom.get_building(building_class)
            building.finish_construction()
        except BuildingNotBuilt:
            building = building_class()
            building.finish_construction()
            
            kingdom.buildings.append(building_class())
        
        kingdom.remove_task(task)
        
        await bot.send_message(channel, "The building {name} is ready !".format(name=building_name))
    except DiscordSTRException as e:
        await bot.say(e)
