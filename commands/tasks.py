import time
from common import *


@bot.command(pass_context=True)
async def tasks(context, page=1):
    """Shows all the work in progress in your kingdom.
    
    `?tasks` list all pending task in your kingdom (enrolling units, building buildings, ...)
    """
    try:
        author = context.message.author
        users = bot.data['users']
        
        user, kingdom = kingdom_required(author, users)
    
        # No tasks awaiting
        if len(kingdom.tasks) <= 0:
            await bot.say("No ongoing tasks...")
            return
        
        # List tasks
        ongoing_tasks = ""
        for task in kingdom.tasks:
            elapsed_time = time.time() - task.started_at
            time_left = task.duration - elapsed_time
            
            ongoing_tasks += "+ {task.desc}\t\t{task.workforce}\t\t{time_left}s\n".format(task=task, time_left=int(time_left))
        
        await bot.say(("```diff\n" +
            "!===== [{user.title} {user.name}'s Tasks ({page}/{page_count}) ] =====!\n" +
            "% Task\tMinions\tTime left\n" + 
            ongoing_tasks +
            "!=========================================!" +
            "```").format(page=1, page_count=1, user=user))
    except DiscordSTRException as e:
        await bot.say(e)