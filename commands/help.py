from common import *
from commands.commands import _commands


@bot.command()
async def help(command=""):
    """Shows the help menu.
    
    To get the list of all availables commands, type '?commands', use '?commands <command>' for more informations about a given command
    """
    if command == "":
        await bot.say("**DiscordSTR** let you rule a marvelous kingdom from scratch ! Start your journey as a kingdom's ruler by typing `?kingdom`. To get the list of all availables commands, type `?commands`, use `?commands <command>`, or `?help <command>` for more informations about a given command.")
    else:
        await _commands(command)
