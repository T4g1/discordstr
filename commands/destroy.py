from common import *


@bot.command(pass_context=True)
async def destroy(context, confirmation=""):
    """This command allows you to crush your kingdom TO THE GROUND !
    
    Use it only if you wish to re-start a brand new kingdom.
    `?destroy greenlamp_sucks` to skip the confirmation message
    """
    quotes = [
        "Let such madness never happen again",
        "Mankind is so foolish",
        "Godzilla would'nt have done a better job",
        "Lets hope those poor fela will live a better life in afterlife",
        "All your base are belong to us",
        "All of them..."
    ]
    
    try:
        author = context.message.author
        users = bot.data['users']
        
        user, kingdom = kingdom_required(author, users)
        
        if confirmation == "greenlamp_sucks":
            await bot.say("The migthy {user.title} {user.name} just crushed its kingdom to the ground, killing all inhabitants of the {kingdom.name} kingdom... {quote}".format(user=user, kingdom=kingdom, quote=random.choice(quotes)))
            
            bot.data['users'][author.id].kingdom = None
        else:
            await bot.say("Beware migthy {user.title} {user.name}, you are about to crush your kingdom to ground ! You will have to settle another kingdom after doing so if you wish to keep playing this wonderfull game. Confirm that such intention is truly yours by typing `?destroy greenlamp_sucks` (There is no coming back)".format(user=user))
    except DiscordSTRException as e:
        await bot.say(e)
