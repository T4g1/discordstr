import unittest
import math
from classes.ressources import Ressources
from common import *
from commands.kingdom import kingdom as _kingdom
from commands.cheat import _cheat as _cheat
from units.minion import Minion


class FakeAuthor():
    def __init__(self):
        self.id = 1
        self.name = "test"
        self.mention = "test"

class FakeChannel():
    def __init__(self):
        self.id = 1


class FakeMessage():
    def __init__(self):
        self.author = FakeAuthor()
        self.channel = FakeChannel()


class FakeContext():
    def __init__(self):
        self.message = FakeMessage()


class DiscordSTRTestCase(unittest.TestCase):
    def setUp(self):
        """Called before each test"""
        self.loop = asyncio.new_event_loop()
        asyncio.set_event_loop(self.loop)

        self.context = FakeContext()
        
        bot.set_debugging(True, verbose=False)
        bot.init_data()

    def tearDown(self):
        """Called after each test"""
        self.end_tasks()
        
        self.loop.run_until_complete(bot.logout())

    def get_kingdom(self):
        """Shortcut to get the kingdom"""
        author = self.context.message.author
        users = bot.data['users']
        
        user, kingdom = kingdom_required(author, users)
        
        return kingdom
    
    def generate_kingdom(self):
        """Generates a dummy kingdom"""
        self.loop.run_until_complete(_kingdom.callback(self.context, "Jean de la roulette russe"))
    
    def large_ressources(self):
        """Generates large ressources
        
        Don't use math.inf as it raise exception when divided !
        """
        kingdom = self.get_kingdom()
        
        kingdom._ressources = Ressources(
            gold=1000000000000000,
            food=1000000000000000,
            wood=1000000000000000,
            stone=1000000000000000
        )

    def large_workforce(self):
        """Generate a large workforce for testing purpose
        
        Don't use math.inf as it is a float and a number of units can't be a float !
        """
        kingdom = self.get_kingdom()
        
        unit = Minion()
        unit.ready = 100000
        
        kingdom.units.add_unit(unit)
    
    def build_all_buildings(self):
        """Builds all building"""
        kingdom = self.get_kingdom()
        
        for building_class in l_buildings:
            building = building_class()
            building.finish_construction()
            
            kingdom.buildings.append(building)
            
    
    def end_tasks(self):
        """Shortcut to end all current tasks"""
        self.loop.run_until_complete(_cheat.callback(self.context, "endall"))
        
        # Wait for the completion of the function that comes after the sleep
        pending = asyncio.Task.all_tasks()
        for task in pending:
            if task._exception != None:
                continue
            try:
                self.loop.run_until_complete(asyncio.gather(task))
            except asyncio.CancelledError:
                # Our sleeping process in task raise that error
                pass


if __name__ == '__main__':
    """Ran test with:
    python test.py
    from root directory of DiscordSTR
    """
    
    suite = unittest.TestLoader().discover('.', pattern="*_test.py")
    unittest.TextTestRunner(verbosity=2).run(suite)
